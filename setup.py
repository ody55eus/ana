# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from setuptools import setup, find_packages

__author__ = "Jonathan Pieper"
__version__ = '0.4'
__license__ = 'GNU GPLv3'

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

setup_requirements = [
    "h5py==3.9.0",
    "matplotlib==3.7.1",
    "numpy==1.25.2",
    "pandas==2.1.0",
    "scipy==1.11.2",
    "seaborn==0.12.2",
    "pycairo==1.24.0",
    "tabulate==0.9.0"]

test_requirements = ['coverage', ]

setup(
    name="ana",
    version=__version__,
    author=__author__,
    author_email="ody55eus@mailbox.org",
    description='Analyze and visualize your measurements',
    keywords='ana, spectrum, fft, signal processing, '
             'scipy, pandas, statistics',
    license=__license__,
    url='https://gitlab.com/ody55eus/ana',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Experimenter',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Topic :: Scientific/Engineering :: Physics',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    packages=find_packages(include=['ana']),
    setup_requires=setup_requirements,
    install_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
)
