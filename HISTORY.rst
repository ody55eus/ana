=======
History
=======

0.4
---

* Updated to Python 3.10 and Matplotlib 3.7.1.
* Freezing software versions in requirements.txt.
* Reproduced Results with current software versions.
* Removed `GitLab SAST`_ (Static Application Security Testing).
* Added code coverage.

0.3
---

* Submitted with master thesis.
* Updated documentation.
* Added figures from resulting analysis.
* Updated some links.

0.2.6 alpha
-----------

* Updated documentation.
* Added new method to (MFN and RAW):
  ``subtract_mean`` to subtract the mean of a measurement.
* Added setup requirements.
* Added `GitLab SAST`_ (Static Application Security Testing)

.. _GitLab SAST: https://docs.gitlab.com/ee/user/application_security/

0.2.5 (2020-09-02)
------------------

* Added Documentation
* Improved functionality
* First GNU GPLv3 Licensed release.

0.2.4 (2020-08-20)
------------------

* Added Documentation
* Improved functionality
* GNU LGPLv3 Licensed release.

0.2.2 (2020-07-20)
------------------

* Structuring and analyzing Data.


0.2.0 (2020-03-20)
------------------

* Visualising plots for Presentations.

0.1.0 (2020-01-20)
------------------

* First release on GitLab.
