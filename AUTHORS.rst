=======
Credits
=======

Creator and Maintainer
~~~~~~~~~~~~~~~~~~~~~~

* Jonathan Pieper <ody55eus@mailbox.org>


Supervisor
~~~~~~~~~~

* `Prof. Dr. Jens Müller <https://www.uni-frankfurt.de/49965558/>`_
 - Valuable input and suggestions to the measurements


Contributors
~~~~~~~~~~~~~

* [Your name or handle] <[email or website]>
 - [Brief summary of your changes]

