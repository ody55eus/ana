.. highlight:: shell

============
Contributing
============

Contributions are welcome, and they are greatly appreciated! Every help is appreciated, and credit will always be given.

You can contribute in many ways:

Types of Contributions
----------------------

Report Bugs
~~~~~~~~~~~

Report bugs at https://gitlab.com/ody55eus/ana/-/issues.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

Fix Bugs / Implement Features
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Look through the GitLab issues for bugs. Anything tagged with "bug", "enhancement" and "help
wanted" is open to whoever wants to implement it.

Write Documentation
~~~~~~~~~~~~~~~~~~~

Ana could always use more documentation, whether as part of the
official Ana docs, in docstrings, or even on the web in blog posts,
articles, and such.

Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an issue at https://gitlab.com/ody55eus/ana/-/issues.

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

Get Started!
------------

Ready to contribute? First, set up ``ana`` for local development.
See the :doc:`installation guide <install>` for help.

#. Fork the `ana` repo on GitLab.

#. Install your local copy ``python setup.py develop``

#. Create a branch for local development::

    $ git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

#. When you're done making changes, check that your changes pass flake8 and the
   tests, including testing other Python versions (or automate this with tox)::

    $ python -m unittest discover -s tests/*

#. Commit your changes and push your branch to GitLab::

    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature

7. Submit a merge request through the GitLab website.


Merge Request Guidelines
-----------------------

Before you submit a merge request, check that it meets these guidelines:

1. The merge request should include tests.
2. If the merge request adds functionality, the docs should be updated. Put
   your new functionality into a function with a docstring, and add the
   feature to the list in README.rst.
3. GitLab CI/CD will automatically test your code and the request can be merged
   if all tests passes.


Developer's Certificate of Origin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By making a contribution to this project, you certify with the
`following statement <https://developercertificate.org/>`_ that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.


Tips
----

To run a subset of tests run one of the following lines::

    $ python -m unittest discover -s tests/basic
    $ python -m unittest discover -s tests/ana/prepair
    $ python -m unittest discover -s tests/ana/fit
    $ python -m unittest discover -s tests/ana/visualize

Deploying
---------

A reminder for the maintainers on how to deploy.
Make sure all your changes are committed (including an entry in HISTORY.rst
and changed version numbers in files).
Then merge these changes into the master branch and create a new release using GitLab.
