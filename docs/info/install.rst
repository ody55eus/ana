.. highlight:: shell

============
Installation
============


Stable release
--------------

Get the latest code:

.. code-block:: console

    $ apt update && apt install git -y
    $ git clone --recursive https://gitlab.com/ody55eus/ana.git

To install the `spectrumanalyzer`_ and `Ana`_, run this command in your terminal:

.. _spectrumanalyzer: /modules/spectrumanalyzer
.. _Ana: /modules/ana

.. code-block:: console

    $ python setup.py install

This is the preferred method to install Ana, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Ana can be downloaded from the `GitLab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://gitlab.com/ody55eus/ana.git

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/ody55eus/ana/-/archive/master/ana-master.zip

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ pip install -e . # --editable or 
    $ python setup.py develop  # or install


.. _GitLab repo: https://gitlab.com/ody55eus/ana
.. _tarball: https://gitlab.com/ody55eus/ana/-/archive/master/ana-master.zip
