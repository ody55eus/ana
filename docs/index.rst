.. documentation master file, created by
   sphinx-quickstart on Thu Aug 20 01:21:35 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Data Analysis Framework (Ana)
=============================

.. toctree::
   :maxdepth: 3
   :caption: General Information

   readme
   info/install


.. toctree::
   :maxdepth: 5
   :caption: Programming Framework API

   info/usage
   programming


.. toctree::
   :maxdepth: 3
   :caption: Additional Information

   info/license
   contributing
   authors
   history
   info/idx
