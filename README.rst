======
Readme
======


Purpose
~~~~~~~~~~

This framework analyzes measurements performed with EVE.
It reads data from comma-separated value (csv) files and provides various tools for analysis.



Running the code
~~~~~~~~~~~~~~~~

This repository contains the Analysis Framework (Ana), which installs with the `setup.py`:

.. code-block:: bash
   :caption: Simple Installation

    $ python setup.py install


Additionally, the scripts to evaluate the data can be found in the folder src.



There are also some IPython Notebooks to visualize and interact with the output in the folder notebooks.


Using the Notebooks with Docker
-------------------------------

The Ana source repository is available on GitLab.
The easiest way is to install Docker and run the notebook server as a Docker container.

.. code-block:: bash
   :linenos:
   :caption: This command will start a vitual machine, clone the repository, and install all dependencies:
   
   docker run -d -p 8888:8888 -e "PASSWORD=YourSecurePassword" \
                -e GIT_URL="https://gitlab.com/ody55eus/ana.git" \
                registry.gitlab.com/ganymede/jupyterlab:latest


Then insert the URL into the Webbrowser: http://localhost:8888 and enter the chosen Password (Here: ``YourSecurePassword``).


Development Install
-------------------

Run the following command from the repository root to install the master version of Ana with the option ``-e`` (short for ``--editable``):

.. code-block:: bash
   :linenos:
   :caption: Inside the notebook server, create a terminal and install the SpectrumAnalyzer and Ana with the following lines:

    python -m pip install -e .

This downloads Ana from this repository and installs the framework.

You can also browse through the notebooks using the original Jupyter notebook interface: http://localhost:8888/tree/notebooks



UML Diagram
~~~~~~~~~~~

The unified markup language (UML) provides great tools to visualize the structure of this framework: 

|Diagram|

.. |Diagram| image:: img/ana_uml.svg


Style Guide
~~~~~~~~~~~

This code uses the Python style guide (:pep:`8`).
Every contribution is encouraged to comply with this style guide.

License
~~~~~~~

.. image:: https://upload.wikimedia.org/wikipedia/commons/9/93/GPLv3_Logo.svg
   :width: 127px
   :target: https://www.gnu.org/licenses/gpl-3.0.html

.. image:: https://opensource.org/files/OSI_Approved_License.png
   :width: 150px
   :target: https://opensource.org/licenses/gpl-3.0

.. image:: https://upload.wikimedia.org/wikipedia/commons/b/b7/Approved-for-free-cultural-works.svg
   :width: 144px
   :target: https://creativecommons.org/share-your-work/public-domain/freeworks


The analysis framework Ana is distributed under the terms of the GNU GPLv3 License to
explicitly grant permission and **encourage users to use, modify and share**
the code under the named terms.

.. note::
    
   Copyright (C) 2020 Jonathan Pieper

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
