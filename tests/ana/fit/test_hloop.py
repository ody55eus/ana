# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import ana


class HloopTestCase(unittest.TestCase):
    def test_hloop_init(self):
        hloop = ana.Hloop(57)

    def test_hloop_fit(self):
        hloop = ana.Hloop(57)
        hloop.fit()


if __name__ == '__main__':
    unittest.main()