# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import ana


class RAWTestCase(unittest.TestCase):
    def setUp(self):
        if not os.path.exists('output'):
            os.makedirs('output')

    def create_raw_measurement(self, kind='normal'):
        """
        Args:
            kind:
        """
        data = create_timesignal(kind=kind)
        self.raw = ana.RAW(data=data, rate=8)
        self.raw.style.set_style(notebook=True)

    def test_calc_first_spectrum(self):
        for kind in ['normal',
                     'randwalk',
                     'linear']:
            self.create_raw_measurement(kind=kind)
            self.raw.calc_first_spectrum()

            self.assertFalse(self.raw.avg_spec.empty)

    def test_calc_second_spectrum(self):
        for kind in ['normal',
                     'randwalk', ]:
            self.create_raw_measurement(kind=kind)
            self.raw.calc_first_spectrum()
            self.raw.calc_second_spectrum(highest_octave=1)

            self.assertFalse(self.raw.avg_spec.empty)
            self.assertTrue(self.raw.second_spectrum_time_array.any())

    def test_plot_first_spectrum(self):
        for kind in ['normal',
                     'randwalk', ]:
            self.create_raw_measurement(kind=kind)
            self.raw.calc_first_spectrum()
            self.raw.calc_second_spectrum(highest_octave=1)

            fig, ax = plt.subplots()
            self.raw.plot_spectrum(ax)
            ax.legend(loc='lower left')

            par1 = ax.twinx().twiny()
            self.raw.plot_time(par1)
            par1.legend(loc='upper right')

            plt.show()
            plt.savefig('output/first_spectrum_test_%s.png' % kind)
            plt.close()


def create_timesignal(n=2 ** 15, kind='normal', seed=42):
    """
    Args:
        n:
        kind:
        seed:
    """
    if seed:
        np.random.seed(seed)

    if kind == 'normal':
        timesignal = np.random.randn(n)
    elif kind == 'randwalk':
        timesignal = np.random.randn(n).cumsum()
    elif kind == 'linear':
        timesignal = np.linspace(0, 1, num=n)
    else:
        raise NameError("Unknown Kind: %s" % kind)

    data = {'data': pd.Series(timesignal),
            'info': {'Nr': 0,
                     'random': kind,
                     }}
    return data


if __name__ == '__main__':
    unittest.main()
