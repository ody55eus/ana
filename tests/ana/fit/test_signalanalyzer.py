# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
import os
import numpy as np
import pandas as pd

import ana


class SATestCase(unittest.TestCase):
    def setUp(self):
        self.raw = create_raw_measurement()
        filename = 'output/'
        if not os.path.exists(filename):
            os.makedirs(filename)

    def test_calc_first_spectrum(self):
        self.raw.calc_first_spectrum()

        self.assertFalse(self.raw.avg_spec.empty)

    def test_initialize_SA(self):
        self.raw.calc_first_spectrum()
        spec = self.raw.avg_spec
        df = pd.DataFrame({'f': spec.freq,
                           'Vx': spec.S,
                           'Vy': 0})
        self.sa = ana.SA(df)


def create_raw_measurement():
    data = create_timesignal()
    return ana.RAW(data=data, rate=8)


def create_timesignal(n=2 ** 15, kind='normal', seed=42):
    """
    Args:
        n:
        kind:
        seed:
    """
    if seed:
        np.random.seed(seed)

    if kind == 'normal':
        timesignal = np.random.randn(n)
    elif kind == 'randwalk':
        timesignal = np.random.randn(n).cumsum()
    elif kind == 'linear':
        timesignal = np.linspace(0, 1, num=n)
    else:
        raise NameError("Unknown Kind: %s" % kind)

    data = {'data': pd.Series(timesignal),
            'info': {'Nr': 0,
                     'random': kind,
                     }}
    return data


if __name__ == '__main__':
    unittest.main()
