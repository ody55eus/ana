# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest

import logging  # System Modules
import os
from glob import glob

# Basic Plotting libraries
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from matplotlib import cm
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

# Math / Science Libraries
import pandas as pd
import numpy as np
import scipy
import scipy.stats

import ana

logging.basicConfig(level=logging.ERROR)


def load_data(datapath, focus='Vin', cut=-2):
    meas_data = {}
    meas_info = {}
    all_data = {}
    for f in datapath:
        f_info = ana.measurement.MeasurementClass().get_info_from_name(f)
        sr = f_info[focus]
        nr = f_info['nr']
        meas_info[sr] = f_info
        meas_data[sr] = pd.read_csv(f, sep='\t')
        new_df = meas_data[sr]
        new_df[focus] = float(sr[:cut])
        if nr in all_data.keys():
            all_data[nr] = pd.concat([all_data[nr], new_df])
        else:
            all_data[nr] = new_df
    return meas_data, meas_info, all_data


def calc_PSD(meas_data):
    meas_obj = {}
    for sr, data_df in meas_data.items():
        if len(data_df['Vx']) % 1024:
            avg = len(data_df['Vx']) // 1024
            d = data_df['Vx'].iloc[:-(len(data_df['Vx']) % 1024)]
        else:
            d = data_df.Vx

        max_len = len(d)

        data = {
            'data': d,
            'info': {
                'Nr': 0,
                'rate': 1 / data_df.time.diff().mean(),
                'length': max_len * data_df.time.diff().mean(),
            }
        }

        meas_obj[sr] = ana.RAW(data,
                               rate=data['info']['rate'],
                               nof_first_spectra=32,
                               calc_first=True,
                               downsample=False,
                               )
    return meas_obj


def merge_data(meas_obj, cutoff_frequency=.9, cut=-2):
    diff_voltages = pd.DataFrame()
    for sr, m in meas_obj.items():
        s = m.avg_spec
        s = s[s.freq < cutoff_frequency]
        if len(s) < 2:
            continue
        newdf = pd.DataFrame()
        newdf['freq'] = s.freq
        newdf['S'] = s.S
        newdf['Vin'] = float(sr[:cut])
        diff_voltages = pd.concat([diff_voltages, newdf])
    return diff_voltages


def plot_PSD_classic(diff_voltages, title, groupby_category='SR', group_name='T/min',
                     num=10, style=[['science'], {'context': 'talk', 'style': 'white', 'palette': 'bright', }]):
    c1 = sns.color_palette("hls", num)
    sns.set_palette(c1)
    fig, ax = plt.subplots(figsize=(16, 12))
    # g = sns.relplot(x='freq', y='S', hue='Vin', data=diff_voltages, height=5, kind='line')
    grouped = diff_voltages.groupby(groupby_category)
    for group in grouped.groups.keys():
        if group == 0:
            grouped.get_group(group).plot(x='freq', y='S', kind='line',
                                          loglog=True, ax=ax,
                                          label='%.1e %s' % (group, group_name),
                                          color='black',
                                          xlabel='Frequency [Hz]',
                                          ylabel='$S_{V_H}$ [$\\mathrm{V}^2/\\mathrm{Hz}$]',
                                          )
        else:
            grouped.get_group(group).plot(x='freq', y='S', kind='line',
                                          loglog=True, ax=ax,
                                          label='%.1e %s' % (group, group_name),
                                          xlabel='Frequency [Hz]',
                                          ylabel='$S_{V_H}$ [$\\mathrm{V}^2/\\mathrm{Hz}$]',
                                          )
    ax.set_title(title)
    # save_plot('m506', 'png')

    return ax


def show_PSD_classic(diff_voltages, title, ax=None, groupby_category='Vin', group_name=r'$\mu A$',
                     num=10, style=[['science'], {'context': 'talk', 'style': 'white', 'palette': 'bright', }]):
    if not ax:
        fig, ax = plt.subplots(figsize=(12, 8))
    c1 = sns.color_palette("hls", num)
    sns.set_palette(c1)
    # g = sns.relplot(x='freq', y='S', hue='Vin', data=diff_voltages, height=5, kind='line')
    grouped = diff_voltages.groupby(groupby_category)
    for group in grouped.groups.keys():
        grouped.get_group(group).plot(x='freq', y='S', kind='line',
                                      loglog=True, ax=ax,
                                      label='%s %s' % (group * 1e-3, group_name),
                                      xlabel='Frequency [Hz]',
                                      ylabel='$S_{V_H}$ [$\\mathrm{V}^2/\\mathrm{Hz}$]',
                                      )
    ax.set_title(title)
    return ax


def plot_PSD_contour(meas_obj, diff_voltages, title,
                     cutoff_frequency=.9,
                     groupby_category='Vin'):
    diff_voltages_contour = pd.DataFrame()
    for sr, m in meas_obj.items():
        s = m.avg_spec
        s = s[s.freq < cutoff_frequency]
        if len(s) < 2:
            continue
        diff_voltages_contour[float(sr[:-2])] = s.S

    v = diff_voltages[groupby_category].unique()
    v.sort()
    frequencies = diff_voltages.freq.unique()
    smin, smax = diff_voltages.S.min(), diff_voltages.S.max()
    levels = np.logspace(np.log10(smin),
                         np.log10(smax), 10)

    fig, ax = plt.subplots(figsize=(12, 8))
    cs = ax.contourf(v, frequencies, diff_voltages_contour,
                     norm=LogNorm(vmin=smin, vmax=smax),
                     levels=levels,
                     cmap=plt.cm.Blues,
                     )
    cbar = plt.gcf().colorbar(cs, ax=ax)
    cbar.set_label('$S_V^{V_{in}} (f)$')
    cbar.set_ticklabels(['%.1e' % _ for _ in levels])
    ax.set_yscale('log')
    ax.set_ylabel('$f$ [Hz]')
    ax.set_xlabel('$V_{in}$ [$m$V]')
    ax.set_title(title)


def plot_diff_volt(nr=508, figsize=(16, 12)):
    datapath = glob(f'./data/MFN/m{nr}/*')
    meas_data, meas_info, all_data = load_data(datapath)
    meas_obj = calc_PSD(meas_data)
    diff_voltages = merge_data(meas_obj)

    sns.set('talk', 'ticks')
    plt.rcParams['axes.grid'] = True
    plt.rcParams['grid.linestyle'] = '--'

    c1 = sns.color_palette("hls", 10)
    sns.set_palette(c1)
    fig, ax = plt.subplots(figsize=figsize)
    show_PSD_classic(diff_voltages,
                     'Current Amplitudes (SR830DAQ, m%s)' % nr,
                     ax=ax)

    key = next(iter(meas_obj.keys()))
    meas_obj[key].style.draw_oneoverf(ax, ymin=1e-13, xmin=1.7e-1,
                                      alpha=2, factor=6,
                                      plot_style='b--', an_color='blue')

    ax.grid(visible=True, which='minor',
            color='#cccccc', linestyle='-.',
            alpha=.3)

    inset2 = inset_axes(ax, width='100%', height='100%',
                        bbox_to_anchor=(.54, .75, .3, .25),
                        bbox_transform=ax.transAxes)
    inset3 = inset_axes(ax, width='100%', height='100%',
                        bbox_to_anchor=(.1, .1, .3, .25),
                        bbox_transform=ax.transAxes)

    grouped = diff_voltages.groupby('Vin')
    fit_dict = dict()
    for group in grouped.groups.keys():
        g = grouped.get_group(group)
        fit_area = g.query('freq > %f and freq < %f' % (2e-2, 7e-1))
        fit_area['lnf'] = np.log10(fit_area.freq)
        fit_area['lnS'] = np.log10(fit_area.S)
        fit = scipy.stats.linregress(fit_area.lnf, fit_area.lnS)
        intercept, slope = fit.intercept, -fit.slope
        voltage = group * 1e-3
        fit_dict[voltage] = {'alpha': slope,
                             'amplitude': 10 ** intercept}

        inset2.plot(voltage, 10 ** intercept, 'o')
        inset3.plot(voltage, slope, 'o')

    inset2.set_xlabel(r'Current [$\mu\mathrm{A}$]')
    inset2.set_ylabel('$S_{V_H} (f=1\\;$Hz$)$')
    inset2.set_yscale('log')

    inset3.set_xlabel(r'Current [$\mu\mathrm{A}$]')
    inset3.set_ylabel('$\\alpha$')

    return diff_voltages, fit_dict


def plot_diff_sweeprates(nr=507, figsize=(16, 12)):
    datapath = glob(f'./data/MFN/m{nr}/*')
    meas_data, meas_info, all_data = load_data(datapath, focus='SR', cut=None)
    meas_obj = calc_PSD(meas_data)
    f_max = (8 / (2 * np.pi))
    diff_voltages = merge_data(meas_obj, cutoff_frequency=f_max, cut=None)
    title = 'Sweeprates (SR830DAQ, m%s)' % nr
    #($\\tau = 100~\\mathrm{ms}$; $f_{Ref} = 17~\\mathrm{Hz}$)'

    sns.set('talk', 'ticks')
    plt.rcParams['axes.grid'] = True
    plt.rcParams['grid.linestyle'] = '--'

    ax = plot_PSD_classic(diff_voltages, title, groupby_category='Vin', num=3)

    key = next(iter(meas_obj.keys()))
    meas_obj[key].style.draw_oneoverf(ax, ymin=1e-13, xmin=2e-1,
                                      alpha=2, factor=6,
                                      plot_style='b--', an_color='blue')
    ax.grid(visible=True, which='minor',
            color='#cccccc', linestyle='-.',
            alpha=.3)


    inset2 = inset_axes(ax, width='100%', height='100%',
                        bbox_to_anchor=(.15, .5, .3, .25),
                        bbox_transform=ax.transAxes)
    inset3 = inset_axes(ax, width='100%', height='100%',
                        bbox_to_anchor=(.08, .1, .3, .24),
                        bbox_transform=ax.transAxes)

    grouped = diff_voltages.groupby('Vin')
    fit_dict = dict()
    for group in grouped.groups.keys():
        g = grouped.get_group(group)
        fit_area = g.query('freq > %f and freq < %f' % (8e-2, 7e-1))
        fit_area['lnf'] = np.log10(fit_area.freq)
        fit_area['lnS'] = np.log10(fit_area.S)
        fit = scipy.stats.linregress(fit_area.lnf, fit_area.lnS)
        intercept, slope = fit.intercept, -fit.slope

        if group == 0:
            continue

        voltage = group * 1e3
        fit_dict[voltage] = {'alpha': slope,
                             'amplitude': 10 ** intercept}

        inset2.plot(voltage, 10 ** intercept, 'o')
        inset3.plot(voltage, slope, 'o')

    inset2.set_xlabel('Sweeprate $\\frac{d}{dt} \mu_0 H_{ext} [\\mathrm{mT}/\\mathrm{min}$]')
    inset2.set_ylabel('$S_{V_H} (f=1\\,$Hz$)$')
    inset2.set_yscale('log')

    inset3.set_xlabel('Sweeprate $\\frac{d}{dt} \mu_0 H_{ext} [\\mathrm{mT}/\\mathrm{min}$]')
    inset3.set_ylabel('$\\alpha$')

    return diff_voltages, fit_dict


def plot_compare_time(m, fields, add='',
                      figsize=(16,12),
                      colors=['red', 'blue', 'green']):
    fig, axes, tax = m.plot_compare_timesignals(fields,
                                                figsize=figsize,
                                                subplot_kw=dict(
                                                    gridspec_kw=dict(
                                                        wspace=.16,
                                                    )
                                                ),
                                                colors=colors
                                                )
    for i, ax in enumerate(axes):
        ax.set_xlabel('Time $t$ [s]')
        tax[i].set_xlabel(r'\Large KDE', fontdict={'fontsize': 64,
                                                   'fontweight': 'bold'})
        if i == 0:
            ax.set_ylabel('$V_H$ [mV]')

    #plt.suptitle(r'Timesignals inside the Hysteresis %s' % add)


def fit_data(mfn):
    df_fit = pd.DataFrame()
    for field in np.sort(mfn.data.Field.unique()):
        signal = mfn.data.query('Field == @field')
        mean = signal.Vx.mean()
        signal.loc[:, 'fittedVx'] = signal.Vx - mean

        df_fit = pd.concat([df_fit, signal])
    return df_fit


def split_data(mfn, df_fit):
    neg_saturation = mfn.data.Field.min()
    field_range = mfn.data.Field.max() - mfn.data.Field.min()
    steps = 4
    df = pd.DataFrame()
    for i in range(steps):
        stepsize = (field_range / steps)
        start = neg_saturation + i * stepsize
        end = start + stepsize
        df_tmp = df_fit.query('Field >= @start and Field < @end')
        df_tmp.reset_index(inplace=True)
        df_tmp['Group'] = i
        for j, f in enumerate(np.sort(df_tmp.Field.unique())):
            df_tmp.loc[df_tmp.query('Field == @f').index, 'SubGroup'] = j
            df_tmp.loc[df_tmp.query('Field == @f').index, 'x'] = df_tmp.loc[
                       df_tmp.query('Field == @f').index, 'fittedVx'] + i * .2
        df = pd.concat([df, df_tmp])
    return df


def plot_hist(mfn,
              filename = '',
              factor = 1e3,
              steps = 4,
              asc = True):
    # plt.style.use(['science'])
    sns.set('paper', 'white', rc={"axes.facecolor": (0, 0, 0, 0),
                                  'figure.figsize': (6, 4),
                                  })
    neg_saturation = mfn.data.Field.min()
    field_range = mfn.data.Field.max() - mfn.data.Field.min()

    df = split_data(mfn, fit_data(mfn))

    def label_func(x, color, label=''):
        ax = plt.gca()
        try:
            l = int(float(label))
        except Exception:
            l = label
        ax.text(0, .3, '$%s\\,\\mathrm{mT}$' % l,
                fontweight="bold", color=color,
                ha="left", va="center", fontsize=16,
                transform=ax.transAxes)

    for i in range(steps):
        stepsize = round(field_range / steps, 3)
        start = neg_saturation + i * stepsize
        end = start + stepsize
        a, b = ('>=', '<') if asc else ('>', '<=')
        q = 'Field {} @start and Field {} @end'.format(a,b)
        df_tmp = df.query(q).copy()
        df_tmp.loc[:, 'fittedVx'] *= factor
        df_tmp.loc[:, 'Field'] *= 1e3

        # Initialize the FacetGrid object
        pal = sns.dark_palette(color="black", n_colors=1, input='huls')
        g = sns.FacetGrid(df_tmp, row="Field", hue="Field",
                          aspect=5,
                          height=.8, palette=pal)
        g.map(sns.kdeplot, "fittedVx", clip_on=True, shade=True, alpha=1,
              lw=1.5, bw_adjust=.2)
        g.map(sns.kdeplot, "fittedVx", clip_on=True, color="w", lw=2,
              bw_adjust=.2)
        # g.map(sns.distplot, "fittedVx", hist=True, norm_hist=True,
        #       hist_kws={'alpha': .5})
        g.map(plt.axhline, y=0, lw=1, clip_on=True)
        g.map(label_func, "fittedVx")

        # Set the subplots to overlap
        g.fig.subplots_adjust(hspace=-.3)

        # Remove axes details that don't play well with overlap
        g.set_titles("")
        g.set(yticks=[], xlim=(-2.5, 2))  # , xticks=[-100 + 50*_ for _ in range(5)])
        g.despine(bottom=True, left=True)
        unit = 'mV' if factor == 1e3 else '\\muV' if factor == 1e6 else 'V'
        plt.gca().set_xlabel('$\\Delta V_H$ [$\\mathrm{%s}$]' % unit)
        if filename:
            plt.savefig('output/%s' % (filename.format(i)))


class PlotMasterCase(unittest.TestCase):
    def setUp(self):
        self.meas = {}

        filename = 'output/'
        if not os.path.exists(filename):
            os.makedirs(filename)

        # figures?
        self.save_figures = True
        self.ext = 'pdf'  # or png, pgf
        self.figsize = 16, 12
        self.style = dict(
            default=True,
            figsize=self.figsize,
            latex=True,
            grid=True,
            set_mpl_params=True,
        )

        self.m = ana.Hloop(57)
        self.m.style.set_style(**self.style)
        self.plot = ana.plot.Plot()

    def tearDown(self):
        plt.close()
        del(self.m)
        del(self.plot)

    def test_plot_hist(self):
        mfn = ana.MFN(446)
        plot_hist(mfn, 'hist446_{}.pdf', asc=False)
        mfn.data.to_csv('output/hist446.csv')

        mfn = ana.MFN(447)
        plot_hist(mfn, 'hist447_{}.pdf')
        mfn.data.to_csv('output/hist447.csv')

    def test_plot_compare_hyst(self):
        sp =0.11
        self.plot.plot_hloops(to_show=[23, 22, 119, 120],
                              xlim=[-150, 150],
                              filename='output/hysteresis_45',
                              plot_args=dict(sharex=True,
                                             #sharey=True,
                                             gridspec_kw=dict(
                                                 wspace=sp,
                                                 hspace=sp
                                                )
                                             )
                              )

        for nr, s in [(23,'+'), (22,'+'),
                     (119,'-'), (120,'-')]:
            self.plot.meas[nr].up_fitted.loc[:,['Time', 'B', 'Vx8', 'Bx8']].to_csv(
                'output/hysteresis_{}45-{}-up.csv'.format(s,nr), index=False)
            self.plot.meas[nr].down_fitted.loc[:,['Time', 'B', 'Vx8', 'Bx8']].to_csv(
                'output/hysteresis_{}45-{}-down.csv'.format(s,nr), index=False)
            B_ext, B_stray = self.plot.meas[nr].get_downminusup_strayfield(
                fitted_data=True)
            pd.DataFrame(
                {'H_ext': B_ext,
                'Delta B': B_stray}
                ).to_csv(
                    'output/hysteresis_{}45-{}-diff.csv'.format(s,nr),
                    index=False)

    def test_plot_compare_hyst_par(self):
        sp = 0.11
        self.plot.plot_hloops_90(mirror=True,
                                 filename='output/hysteresis_90',
                                 plot_args=dict(sharex=True,
                                                gridspec_kw=dict(
                                                    wspace=sp,
                                                    hspace=sp
                                                )
                                                )
                                 )
        for nr, s in [(57,'+'), (155,'-')]:
            self.plot.meas[nr].up_fitted.loc[:,['Time', 'B', 'Vx8', 'Vx9', 'Bx8', 'Bx9']].to_csv(
                'output/hysteresis_{}90-{}-up.csv'.format(s,nr), index=False)
            self.plot.meas[nr].down_fitted.loc[:,['Time', 'B', 'Vx8', 'Vx9', 'Bx8', 'Bx9']].to_csv(
                'output/hysteresis_{}90-{}-down.csv'.format(s,nr), index=False)
            B_ext, B_stray = self.plot.meas[nr].get_downminusup_strayfield(
                fitted_data=True)
            pd.DataFrame(
                {'H_ext': B_ext,
                'Delta B': B_stray}
                ).to_csv('output/hysteresis_{}90-{}-diff.csv'.format(s,nr), index=False)

    def test_plot_compare_multi_hyst_plusses(self):
        insets = [
            ((.05, .05, .35, .3),
             ((-225, 0), (.1, .7), 'blue', .1)),
            ((.05, .64, .3, .35),
             ((-520, -200), (.05, .35), 'green', .1)),
            ((.59, .64, .25, .25),
             ((0, 200), (-.33, .05), 'red', .1)),
        ]
        fig, (ax, iax) = self.plot.plot_hloops_compare_90(
                                to_show=[414 + _ for _ in range(4)],
                                insets=insets,
                                nograd=True
                                )
        plt.savefig('output/hloops_plusses.pdf')

        for nr in [414 + _ for _ in range(4)]:
            self.plot.meas[nr].up_fitted.loc[:,['Time', 'B', 'Vx8', 'Bx8']].to_csv(
                'output/hloops_plusses-{}-up.csv'.format(nr), index=False)
            self.plot.meas[nr].down_fitted.loc[:,['Time', 'B', 'Vx8', 'Bx8']].to_csv(
                'output/hloops_plusses-{}-down.csv'.format(nr), index=False)
            B_ext, B_stray = self.plot.meas[nr].get_downminusup_strayfield(
                fitted_data=True)
            pd.DataFrame(
                {'H_ext': B_ext,
                'Delta B': B_stray}).to_csv('output/hloops_plusses-{}-diff.csv'.format(nr), index=False)

    def test_plot_compare_multi_hyst_crosses(self):
        alpha = .08
        insets = [
            ((.05, .05, .33, .33),
             ((-200, -100), (-.9, -.7), 'red', alpha)),
            ((.05, .64, .31, .35),
             ((-70, -0), (-1.1, -.7), 'blue', alpha)),
            ((.42, .74, .31, .25),
             ((40, 140), (-.9, -.7), 'green', alpha)),
            ((.68, .06, .3, .3),
             ((160, 210), (-.77, -.6), 'yellow', alpha)),
        ]
        fig, (ax, iax) = self.plot.plot_hloops_compare_90(
                            to_show=[414, 415, 416, 417],
                            structure="crosses",
                            ylim=(-1.26, -.3), xlim=(-300, 250),
                            insets=insets,
                            nograd=True,
                            nodiff=True)
        plt.savefig('output/hloops_crosses.pdf')

        for nr in [414 + _ for _ in range(4)]:
            self.plot.meas[nr].up_fitted.loc[:,['Time', 'B', 'Vx9', 'Bx9']].to_csv('output/hloops_crosses-{}-up.csv'.format(nr))
            self.plot.meas[nr].down_fitted.loc[:,['Time', 'B', 'Vx9', 'Bx9']].to_csv('output/hloops_crosses-{}-down.csv'.format(nr))
            up = self.plot.meas[nr].up_fitted
            down = self.plot.meas[nr].down_fitted
            f_up = scipy.interpolate.interp1d(up.B, up.Bx9)
            f_down = scipy.interpolate.interp1d(down.B, down.Bx9)
            B_ext = np.linspace(up.B.min(), up.B.max(), int(1e5))
            B_stray = f_down(B_ext) - f_up(B_ext)
            pd.DataFrame(
                {'H_ext': B_ext,
                'Delta B': B_stray}).to_csv('output/hloops_crosses-{}-diff.csv'.format(nr))

    def test_sa_static(self):
        self.plot.plot_static_fields(filename='output/sr785-static')

    def test_sa_fspans(self):
        self.plot.multiple_fspans(filename='output/sr785-fspans')

    def test_sa_temp(self):
        fit_dict = self.plot.sv_temp_effect(filename='output/sr785-temp')
        pd.DataFrame(fit_dict).T.to_csv('output/sr785-temp-fit.csv',
                                        index_label='Nr')

        for nr in [351, 355, 356, 357, 358, 359]:
            self.plot.eva[nr].df.to_csv(
                'output/sr785-temp-{}.csv'.format(nr),
                index=False
            )

    def test_sa_sweeps(self):
        fit_dict = self.plot.first_sweeps(filename='output/sr785-first')
        pd.DataFrame(fit_dict).T.to_csv('output/sr785-first-fit.csv',
                                        index_label='Nr')
        self.plot.fast_sweeprate(show_numbers=[320, 321, 325, 326,
                                               323, 324, 329, 322,
                                               327, 328, 336, 333,
                                               332, 331],
                                 filename='output/sr785-fast')

        for nr in [362, 363, 364, 366, 365]:
            self.plot.eva[nr].df.to_csv('output/sr785-first-{}.csv'.format(nr),
                                        index=False)

        for nr in [320, 321, 325, 326,
                   323, 324, 329, 322,
                   327, 328, 336, 333,
                   332, 331]:
            self.plot.eva[nr].df.to_csv('output/sr785-fast-{}.csv'.format(nr),
                                        index=False)

    def test_time(self):
        for nr, fields, colors in [(446, [-.04, -.015], ['#A00', '#00A']),
                                   (447, [.04, .015], ['#0a0', '#909'])]:
            mfn = ana.MFN(nr)
            mfn.style.set_style(default=True, grid=False,
                               size='talk', style='ticks',
                               palette='deep', latex=True)
            plot_compare_time(mfn,
                              fields,
                              '(m{})'.format(nr),
                              figsize=(16,9),
                              colors=colors
                              )
            plt.savefig('output/daq-time-{}.pdf'.format(nr))

    def test_time2(self):
        i = 1
        for nr, fields, colors in [(446, [-.005, .01], ['#7DD9C7', '#70B4D7'])]:
            i += 1
            mfn = ana.MFN(nr)
            mfn.style.set_style(default=True, grid=False,
                               size='talk', style='ticks',
                               palette='deep', latex=True)
            plot_compare_time(mfn,
                              fields,
                              '(m{})'.format(nr),
                              figsize=(16,9),
                              colors=colors
                              )
            plt.savefig('output/daq-time-446-{}.pdf'.format(i))

    def test_info(self):
        for f, nr in [(-.015, 446),
                      (.015, 447)]:
            m = ana.MFN(nr)
            m.style.set_style(figsize=(12,12), size='notebook')
            m.plot_info(show_field=f,
                        gridspec_kw=dict(wspace=.3,
                                         hspace=.55),
                        notitle=True,
                        fit_range=2e-1,
                        )
            plt.savefig('output/daq-info-{}.pdf'.format(nr))
            m.measurements[f].avg_spec.loc[:,['freq', 'S']].to_csv(
                'output/daq-info-{}-1.csv'.format(nr), index=False)
            m.spectra_field_contour_df.to_csv('output/daq-info-{}-2.csv'.format(nr),
                                              index_label='freq/Field')
            m.spectra_fit_df.to_csv('output/daq-info-{}-3-4.csv'.format(nr))
            m.time_spectra_field_contour_df.to_csv('output/daq-info-{}-5.csv'.format(nr),
                                                   index_label='freq/time')


    def test_compare_sweeprates(self):
        fit_data = self.plot.compare_sweeprates(filename='output/sr785-sweeprates')
        pd.DataFrame(fit_data).T.to_csv('output/sr785-sweeprates-fit.csv',
                                        index_label='Nr')
        for nr in [382, 354, 351, 355, 353, 352]:
            self.plot.eva[nr].df.to_csv('output/sr785-sweeprates-{}.csv'.format(nr),
                                        index=False)

        sr_df, fit_dict = plot_diff_sweeprates(nr=507, figsize=(16, 12))
        plt.savefig('output/daq-sweeprate.pdf')
        for g, df in sr_df.groupby('Vin'):
            df.to_csv('output/daq-sweeprate-{}.csv'.format(g), index=False)
        sr_df.to_csv('output/daq-sweeprate-all.csv', index=False)
        pd.DataFrame(fit_dict).T.to_csv('output/daq-sweeprate-fit.csv',
                                        index_label='Sweeprate')

    def test_compare_current(self):
        fit_dict = self.plot.compare_voltages(filename='output/sr785-voltages')
        pd.DataFrame(fit_dict).T.to_csv('output/sr785-voltages-fit.csv',
                                        index_label='Nr')
        for i in range(1, 6):
            self.plot.eva[i+377].df.to_csv('output/sr785-voltages-{}.csv'.format(i+377))

        volt_df, fit_dict = plot_diff_volt(nr=506, figsize=(16, 12))
        pd.DataFrame(fit_dict).T.to_csv('output/daq-volt-fit.csv',
                                        index_label='Volt')
        for g, df in volt_df.groupby('Vin'):
            df.to_csv('output/daq-volt-{}.csv'.format(g), index=False)
        volt_df.to_csv('output/daq-volt-all.csv', index=False)
        plt.savefig('output/daq-volt.pdf')


if __name__ == '__main__':
    #unittest.main()
    a = PlotMasterCase()
    a.setUp()
    a.test_time2()
    a.tearDown()
