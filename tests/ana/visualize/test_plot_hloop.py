# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest

import logging  # System Modules
import os

# Basic Plotting libraries
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

# Math / Science Libraries
import pandas as pd
import numpy as np
import scipy
import ana

logging.basicConfig(level=logging.WARNING)


class PlotHloopsCase(unittest.TestCase):
    def setUp(self):
        self.meas = {}

        filename = 'output/'
        if not os.path.exists(filename):
            os.makedirs(filename)

        # figures?
        self.save_figures = True
        self.ext = 'pdf'  # or png, pgf
        self.figsize = 16, 12
        self.style = dict(
            default=True,
            figsize=self.figsize,
            set_mpl_params=True,
        )

        self.eva = ana.HandleM(directory='**/data')
        self.m = ana.Hloop(57)
        self.m.style.set_style(**self.style)

        self.plot = ana.plot.Plot()

        if os.path.exists('data/angles_info.csv'):
            self.a = pd.read_csv('data/angles_info.csv')
        else:
            self.a = pd.DataFrame()

    def test_plot(self):
        self.plot.plot_hloops([54,55], 'output/hloop-compare-1')

    def test_plot_m429(self):
        self.plot.plot_single_hloop(nr=429, xlim=(-300, 300))

    def test_m57_zoomed(self):
        # Global information
        x1, x2 = -750, 750
        y1, y2 = -1.5, .5
        main_color = '#CCCCFF'

        # Inset position and limits
        ## Position = x_pos, y_pos, width, height
        ## unit of positions are in % of frame 
        ## x_pos, ypos points to lower left corner of inset
        i1pos = .69, .3, .3, .3
        i1x1, i1x2 = -100, 100
        i2pos = .65, .8, .34, .2
        i2x1, i2x2 = 530, 620
        i2y1, i2y2 = -.175, 0.05
        i3pos = .055, .65, .25, .3
        i3x1, i3x2 = -650, -250
        i3y1, i3y2 = -.05, .125
        i4pos = .06, .03, .3, .25
        i4x1, i4x2 = -100, 0
        i4y1, i4y2 = -1.35, -.75
        i5pos = .07, .36, .4, .28
        i5x1, i5x2 = -300, 50
        i5y1, i5y2 = -.7, .45

        # Highlighting limits
        h0range, h0color, h0alpha = 25, 'blue', .1
        h1x1, h1x2, h1color, h1alpha = -611, -443, 'orange', .1
        h2x1, h2x2, h2color, h2alpha = -291, -443, 'red', .1
        h3x1, h3x2, h3color, h3alpha = -291.13, 36.56, 'green', .1

        # Create Plot
        fig, ax = plt.subplots(figsize=self.figsize)

        # Plot hysetersis
        self.m.plot_strayfield(ax, 'm57: Strayfield ($90^\\circ$)')

        # Draw Inset 1
        inset = inset_axes(ax, width='100%', height='90%',
                           bbox_to_anchor=i1pos,
                           bbox_transform=ax.transAxes)
        # Highlight 0 / 3
        inset.fill([-h0range, -h0range, h0range, h0range], [y2, y1, y1, y2],
                   h0color, alpha=h0alpha)
        inset.fill([h3x1, h3x1, h3x2, h3x2], [y2, y1, y1, y2], h3color,
                   alpha=h3alpha)

        # Highlight tertiary range
        i1tert = h0range / 3  # Tertiary range
        inset.plot([i1tert, i1tert], [y1, y2], 'b--', alpha=.5)
        inset.plot([-i1tert, -i1tert], [y1, y2], 'b--', alpha=.5)

        self.m.plot_strayfield(inset, '$B \\in (-100, 100)$ mT', nolegend=True)
        inset.set_xlim(i1x1, i1x2)
        inset.set_ylim(y1, y2)

        # Draw Inset 2
        inset2 = inset_axes(ax, width='100%', height='90%',
                            bbox_to_anchor=i2pos,
                            bbox_transform=ax.transAxes)
        self.m.plot_strayfield(inset2, '$B \\in (500, 600)$ mT', nolegend=True)
        inset2.set_xlim(i2x1, i2x2)
        inset2.set_ylim(i2y1, i2y2)

        # Draw Inset 3
        inset3 = inset_axes(ax, width='100%', height='90%',
                            bbox_to_anchor=i3pos,
                            bbox_transform=ax.transAxes)
        self.m.plot_strayfield(inset3, '$B \\in (%s, %s)$ mT' % (i3x1, i3x2),
                               nolegend=True)
        inset3.set_xlim(i3x1, i3x2)
        inset3.set_ylim(i3y1, i3y2)
        # Highlight 1 / 2
        inset3.fill([h1x1, h1x1, h1x2, h1x2], [i3y1, i3y2, i3y2, i3y1],
                    h1color, alpha=h1alpha)
        inset3.fill([h2x1, h2x1, h2x2, h2x2], [i3y1, i3y2, i3y2, i3y1],
                    h2color, alpha=h2alpha)

        # Draw Inset 4
        inset4 = inset_axes(ax, width='100%', height='90%',
                            bbox_to_anchor=i4pos,
                            bbox_transform=ax.transAxes)
        self.m.plot_strayfield(inset4, '$B \\in (%s, %s)$ mT' % (i4x1, i4x2),
                               nolegend=True)
        inset4.set_xlim(i4x1, i4x2)
        inset4.set_ylim(i4y1, i4y2)

        # Draw Inset 5
        inset5 = inset_axes(ax, width='100%', height='90%',
                            bbox_to_anchor=i5pos,
                            bbox_transform=ax.transAxes)
        self.m.plot_strayfield(inset5, '$B \\in (%s, %s)$ mT' % (i5x1, i5x2),
                               nolegend=True)
        inset5.set_xlim(i5x1, i5x2)
        inset5.set_ylim(i5y1, i5y2)
        inset5.fill([h3x1, h3x1, h3x2, h3x2], [i5y2, i5y1, i5y1, i5y2],
                    h3color, alpha=h3alpha)

        # Main Plot limits
        ax.set_xlim(x1, x2)
        ax.set_ylim(y1, y2)

        # Highlight in main plot
        ax.fill([-h0range, -h0range, h0range, h0range], [y1, y2, y2, y1],
                h0color, alpha=h0alpha)
        ax.fill([h1x1, h1x1, h1x2, h1x2], [y1, y2, y2, y1], h1color,
                alpha=h1alpha)
        ax.fill([h2x1, h2x1, h2x2, h2x2], [y1, y2, y2, y1], h2color,
                alpha=h2alpha)
        ax.fill([h3x1, h3x1, h3x2, h3x2], [y1, y2, y2, y1], h3color,
                alpha=h3alpha)

        # Remove x and y labels
        for i, inset_ax in enumerate([inset, inset2, inset3, inset4, inset5]):
            inset_ax.set_xlabel('')
            inset_ax.set_ylabel('')
            inset_ax.set_title('')
            ann_x, ann_xx = inset_ax.get_xlim()
            ann_x += (ann_xx - ann_x) * .05
            ann_yy, ann_y = inset_ax.get_ylim()
            ann_y -= (ann_y - ann_yy) * .20
            inset_ax.text(x=ann_x, y=ann_y, s=chr(i + 97) + ')',
                          fontdict=dict(fontweight='bold', fontsize=24),
                          bbox=dict(boxstyle="round,pad=0.1", fc=main_color,
                                    ec="b", lw=1))

        # Save as image (if needed)
        if self.save_figures:
            self.m.style.save_plot('output/m57_zoomed', self.ext)

        return True


if __name__ == '__main__':
    unittest.main()
