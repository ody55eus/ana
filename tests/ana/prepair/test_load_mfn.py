# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import os
import unittest

import ana
import pandas as pd


def get_max_field_idx(data):
    """
    Args:
        data:
    """
    stat = data.groupby('Field').describe()['Vx']
    return (stat['max'] - stat['min']).idxmax()


class LoadMFNTestCase(unittest.TestCase):
    def setUp(self):
        self.logger = logging.getLogger("LoadMFNTest")

        filename = 'output/'
        if not os.path.exists(filename):
            os.makedirs(filename)

        self.handler = ana.HandleM(directory='data')
        self.handler.style.set_style(notebook=True, set_mpl_params=True)

        self.mfn_measurements = {}

        if os.path.exists('data/mfn_info.csv'):
            self.mfn_info = pd.read_csv('data/mfn_info.csv', index_col=0)
            self.mfn_info.index.name = 'Nr'
        else:
            self.mfn_info = pd.DataFrame()

    def run_overview(self, nr):
        """
        Args:
            nr:
        """
        self.logger.info("Running Nr. %s" % nr)
        tc = self.mfn_info['timeconstant'].loc[nr] if \
            nr in self.mfn_info['timeconstant'] else 1

        self.mfn_measurements[nr] = ana.MFN(nr, timeconstant=tc)
        if self.mfn_measurements[nr].data.empty:
            return

        idxmax = get_max_field_idx(self.mfn_measurements[nr].data)
        # self.mfn_measurements[nr].plot_info(show_field=idxmax, numlevels=20)
        # plt.savefig('output/load_m%s_cont_info.png' % nr)

    def test_content(self):
        # self.assertIn('info', self.handler.data)
        # self.assertIn('data', self.handler.data)
        pass

    def test_overview(self):
        numbers = self.mfn_info.query('Error != True and '
                                      'Dir != "STATIC" and '
                                      'Nr > 480').index.tolist()
        for nr in numbers:
            self.run_overview(nr)

    def test_write_MFN(self):
        for nr, m in self.mfn_measurements:
            m.write('output/mfn/m%s' % nr)

    def test_write_info(self):
        filename = 'output/mfn_info.md'
        f = open(filename, 'w')
        self.mfn_info.to_markdown(f)
        f.close()


if __name__ == '__main__':
    unittest.main()
