# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import ana


class SimpleTestCase(unittest.TestCase):
    def test_simple(self):
        s = ana.SingleM()

        self.assertTrue(hasattr(s, 'linear_regression'))

    def test_simple_2(self):
        mfn = ana.MFN('')
        raw = ana.RAW({})

        self.assertTrue(hasattr(mfn, 'plot_info'))
        self.assertTrue(hasattr(raw, 'rate'))


    def test_simple_3(self):
        m = ana.MultipleM()

        self.assertTrue(hasattr(m, 'query'))


if __name__ == '__main__':
    unittest.main()