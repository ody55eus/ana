# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Analyses simple Measurements and provides easy plotting."""

__author__ = "Jonathan Pieper"
__version__ = '0.2.6'
__license__ = 'GNU GPLv3'

# Single Measurements
from .single import SingleM
from .hloop import Hloop
from .signalanalyzer import SA
from .raw import RAW

# Multiple Measurements
from .multiple import MultipleM
from .handle import HandleM
from .mfn import MFN

from .plotting import PlottingClass
from .plot import Plot
