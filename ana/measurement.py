# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
MeasurementClass
=================

Main parent class for all measurements.
These Measurements are represent by one data variable.


All measurements can be represented by
> m = ana.SingleMeasurement()
> m
{name} (Nr. {Nr))
> print(m)
{name} (Nr. {Nr))
{info.key} {info.value}
{info.key} {info.value}
{info.key} {info.value}
"""

import os
import re
import numpy as np

from .plotting import PlottingClass

class MeasurementClass(object):
    """
        The main measurement class for all measurements.
    """
    def __init__(self):
        """
        .. note:: **MeasurementClass:**
            This Class is the parent of all other measurement classes.


        All measurements have the following variables:
            
        name: str/None
            The name of the Measurement Class (e.g. SA, RAW, etc.)

        info: dict
            Contains informations about the measurement. Typical keys are:                
                Nr: float, the number of the measurement,

        """
        self.name = ''
        self.data = {}
        self.info = {'Nr': 0}
        self.info_file = {}
        self.style = PlottingClass()

    def i(self, *key):
        """
        Get  a specific measurement number information.

        
        :param *key: Will be passed to info.get
        
        :return: info.get(*key)

        """
        return self.info.get(*key)


    __repr__ = lambda self: '%s (Nr. %s)\n' % (self.name, self.i('Nr', 0))

    __str__ = lambda self: '%s (Nr. %s)\n' % (self.name, self.i('Nr', 0)) + \
        '\n'.join(['%s:\t%s' % (key, val) for key, val in self.info.items()])
    
    
    def get_info_from_name(self, name, **kwargs):
        """
        Extracting information from filenames.
        Using a default regex first or trying to extract 'key-value' pairs
        separated by '_'.
    
        :param name: filename that contains informations.
        :param kwargs:
            :str regex: Regular expression for the filename
        :return: dict(key=value)
        """
    
        # Default Regex Structure
        # Nr,     Struct,         deg,        Type1
        # Type2, Field, Date, Time
        # I1, I2, Lock-In (Connections)
        # Voltage In, Resistors (R11, R12)
        # R13, R21
        # Capacitors (C11, C21)
        # Temp
        def_regex = ".*[Mm]([0-9.]*)_([A-Za-z]*)_([0-9.-]*)deg_([A-Za-z]*)_" \
                    "([A-Za-z]*)_*B_([0-9.-]*)T_([0-9]*)_([0-9]*)_I1-([" \
                    "0-9\\-]*)_I2-([0-9\\-]*)_G[PB]I[BP].-([0-9\\-]*)_Vin-([" \
                    "0-9.]*)V_R11-([0-9]*.)O_R12-([0-9.]*.)O_R13-([" \
                    "0-9.]*.)_R21-([0-9.]*.)O_C11-([0-9]*)_C21-([0-9]*)_T-([" \
                    "0-9]*)K.*"
        regex = kwargs.get('regex', def_regex)
        reg = re.match(regex, name)
    
        info_dict = dict()
        if not reg:
            filename = name.split(os.sep)[-1][:-4]
            raw_info = filename.split('_')
            for element in raw_info:
                regex_dict = {
                    'nr': 'm([0-9.]*)',
                    'deg': '((neg)?[0-9.-]*)deg',
                    'dir': '([Uu]p|[Dd]own)',
                    'date': '(20[12][90][0-9]*)',
                    'time': '([012][0-9][0-6]*)',
                    'type1': '(RAW|Hloop)',
                    'type2': '(Parallel|Gradio)',
                    'struct': '([Pp]lusses|[Cc]rosses|[Ee]mpty)',
                    'field': 'p?m?([0-9.-]+)m?T',
                }
                if re.match(regex_dict['field'], element):
                    info_dict['field'] = re.match(regex_dict['field'], element).groups()[0]
                    continue
    
                items = element.split('-')
                if len(items) == 1:
                    for key, single_regex in regex_dict.items():
                        reg = re.match(single_regex, items[0])
                        if reg:
                            info_dict[key] = reg.groups()[0]
                elif len(items) == 2:
                    key, value = items
                    info_dict[key] = value
                elif len(items) > 2:
                    value = '-'.join(items[1:])
                    info_dict[items[0]] = value
        else:
            nr, struct, deg, type1, \
                type2, field, date, time, \
                i1, i2, lock_in, \
                vin, r11, r12, r13, r21, \
                c11, c21, temp = reg.groups()
            try:
                info_dict['nr'] = float(nr)
                info_dict['field'] = float(field)
            except ValueError:
                print('Expected float field and nr, got:'
                                  ' %s, %s' % (field, nr))
            info_dict['srtuct'] = struct
            info_dict['deg'] = deg
            info_dict['type1'] = type1
            info_dict['type2'] = type2
            info_dict['date'] = date
            info_dict['time'] = time
            info_dict['i1'] = i1
            info_dict['i2'] = i2
            info_dict['lock_in'] = lock_in
            info_dict['r11'] = r11
            info_dict['r12'] = r12
            info_dict['r13'] = r13
            info_dict['r21'] = r21
            info_dict['vin'] = vin
            info_dict['c11'] = c11
            info_dict['c21'] = c21
            info_dict['temp'] = temp
   
        self.info_file = info_dict
        
        return info_dict

    def calc_log(self, df, keys=['f', 'Vx', 'Vy']):
        for key in keys:
            df['ln%s' % key] = np.log10(df[key])
