# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# -*- coding: utf-8 -*-
"""
Main plotting class to handle plots.
"""

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import seaborn as sns
import numpy as np

# Math / Science Libraries
import scipy

from .handle import HandleM
from .hloop import Hloop
from .plotting import PlottingClass


class Plot(object):
    def __init__(self):
        """A small class used to plot figures for a presentation."""

        super().__init__()

        self.meas = {}
        self.m = Hloop(57)
        self.eva = HandleM(directory='data/SR785')

    def get_plot(self, **kwargs):
        fig, ax = plt.subplots(nrows=kwargs.get('nrows', 1),
                               ncols=kwargs.get('ncols', 1),
                               figsize=kwargs.get('figsize', (16, 12)),
                               **kwargs.get('plot_args',dict())
                               )
        return fig, ax

    def set_plot_style(self, m=None, **kwargs):
        if m is None:
            m = self.m

        m.style.set_style(size=kwargs.get('size', 'talk'),
                          style=kwargs.get('style', 'ticks'),
                          palette=kwargs.get('palette', 'deep'),
                          grid=kwargs.get('grid', True),
                          latex=kwargs.get('latex', True),
                          figsize=kwargs.get('figsize', (16, 12))
                          )

    def plot_single_hloop(self, nr=54, filename='hloop-single-1',
                          **kwargs):
        """Plots a single measurement. Default: 0deg

        Args:
            nr:
            filename (str, optional): File to write. The default is
                'hloop-single-1'.
            xlim / ylim: limit the plots axes.

        Returns:
            {filename}.pdf
        """
        fig, ax = self.get_plot(**kwargs)

        if nr not in self.meas:
            self.meas[nr] = Hloop(nr)

        self.set_plot_style(self.meas[nr])
        self.meas[nr].plot_strayfield(ax)
        ax.set_xlim(*kwargs.get('xlim', (-250, 250)))
        if kwargs.get('ylim'):
            ax.set_ylim(*kwargs.get('ylim'))

        with sns.color_palette('deep'):
            self.set_plot_style(self.m)
            inset = inset_axes(ax, width='100%', height='90%',
                               bbox_to_anchor=(.64, .06, .35, .35),
                               bbox_transform=ax.transAxes)
            max_b = self.meas[nr].up.B.max()
            inset.plot([-max_b, max_b], [0, 0], 'r--', linewidth=.75)
            B_ext, B_stray = self.meas[nr].get_downminusup_strayfield()
            inset.plot(B_ext, B_stray)
            inset.set_ylabel("$\\Delta B_z\\;[\\mathrm{mT}]$")
            inset.set_title("Difference")
            inset.set_xlim(*kwargs.get('xlim', (-250, 250)))

        plt.savefig("%s.pdf" % filename)

    def plot_hloops(self, to_show=[54, 55], filename='hloop-compare-1',
                    **kwargs):
        """Compares Plusses and Crosses between two angles (4 Subplots).
        Default: 0 and 45 Writes file hloop-compare-1.pdf

        Args:
            to_show (list, optional): List of 4 measurement numbers, 
                defaults to [54, 55]
            filename (str, optional): DESCRIPTION, defaults to 'hloop-compare-1'
            **kwargs:

        Returns:
            None
        """
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks',
                               palette='deep', latex=True)
        fig, axes = self.get_plot(nrows=2, ncols=2,
                                  plot_args=kwargs.get('plot_args',dict()))

        for i, nr in enumerate(to_show):
            ax = axes[i // 2][i % 2]
            if nr not in self.meas:
                self.meas[nr] = Hloop(nr)

            self.set_plot_style(self.meas[nr])
            self.meas[nr].plot_strayfield(ax)
            if nr in [54]:
                ax.set_ylim(0, 4.5)
            elif nr in [55]:
                ax.set_xlim(-300, 300)
                ax.set_ylim(-0.8, 4.45)
            if nr in [22, 23]:
                ax.set_xlim(-150, 150)
                ax.set_ylim(-0.25, 3.5)
            elif kwargs.get('xlim'):
                ax.set_xlim(*kwargs.get('xlim'))
            else:
                ax.set_xlim(-250, 250)
            ax.set_title('')

            with sns.color_palette('deep'):
                self.set_plot_style(self.m)
                inset = inset_axes(ax, width='100%', height='90%',
                                   bbox_to_anchor=(.65, .09, .35, .35),
                                   bbox_transform=ax.transAxes)
                max_b = self.meas[nr].up.B.max()
                inset.plot([-max_b, max_b], [0, 0], '--', color='orange', linewidth=.75)
                B_ext, B_stray = self.meas[nr].get_downminusup_strayfield()
                inset.plot(B_ext, B_stray, color='black')
                inset.set_ylabel("$\\Delta B_z\\;[\\mathrm{mT}]$")
                inset.set_title("Difference")
                inset.set_xlim(-250, 250)
                if nr in [55]:
                    inset.set_xlim(-300, 300)
                if nr in [22, 23]:
                    inset.set_xlim(-150, 150)
                if kwargs.get('xlim'):
                    inset.set_xlim(*kwargs.get('xlim'))
                inset.set_yticks(kwargs.get('diff_yticks', [0, 0.5, 1]))

        for ax, struct in zip(axes[0],
                              ['Plusses', 'Crosses']):
            ax.set_title(struct, fontdict={'fontsize': 32,
                                           'fontweight': 'bold'})
            ax.set_xlabel('')

        for ax in axes[:, 1]:
            ax.set_ylabel('')
            ax.set_yticklabels([])

        for ax, a in zip(axes[:, 0],
         [r'{$\mathbf{\theta = +45^{\circ}}$}',
          r'{$\mathbf{\theta = -45^{\circ}}$}']):
            ax.set_ylabel("%s\n$\\langle B_z \\rangle [\\mathrm{mT}]$" % a,
                          fontdict={'fontsize': 24,
                                    'fontweight': 'bold'})

        axes[0, 0].set_ylim(0,4.8)
        axes[0, 1].set_ylim(0,4.8)
        axes[1, 0].set_ylim(-.1,3.45)
        axes[1, 1].set_ylim(-.1,3.45)

        plt.savefig("%s.pdf" % filename)

    def plot_hloops_95(self,
                       to_show=[42, 43, 32, 34],
                       filename='hloop-compare-95',
                       **kwargs):
        """Compares +95/100/105/110deg with -85/80/75/70deg for (180deg
        Comparison)

        :param filename:Filename to save. The default is 'hloop-compare-95'.
        :type filename: str, optional

        Args:
            to_show (list, optional): Measurement numbers to plot. The default
                is a[95]+a[-85].
            filename:
            **kwargs:

        Returns:
            {filename}.pdf
        """
        fig, axes = plt.subplots(2, 2, figsize=(16, 12))
        for i, nr in enumerate(to_show):
            ax = axes[i // 2][i % 2]
            if nr not in self.meas:
                self.meas[nr] = Hloop(nr)
            self.set_plot_style(self.meas[nr])
            self.meas[nr].plot_strayfield(ax)
            if kwargs.get('xlim'):
                ax.set_xlim(*kwargs.get('xlim'))
            else:
                ax.set_xlim(-750, 750)
            if self.meas[nr].info['Structure'] == 'Crosses' and kwargs.get(
                    'crosses_xlim'):
                ax.set_xlim(*kwargs.get('crosses_xlim'))

            with sns.color_palette('deep'):
                self.m.style.set_style(size='talk', style='ticks',
                                       grid=True, latex=True)
                inset = inset_axes(ax, width='100%', height='90%',
                                   bbox_to_anchor=(.65, .09, .35, .35),
                                   bbox_transform=ax.transAxes)
                max_b = self.meas[nr].up.B.max()
                inset.plot([-max_b, max_b], [0, 0], 'r--', linewidth=.75)
                B_ext, B_stray = self.meas[nr].get_downminusup_strayfield()
                inset.plot(B_ext, B_stray)
                # inset.set_ylabel("$\Delta B_z\\;[\\mathrm{mT}]$")
                inset.set_title("Difference")
                if kwargs.get('xlim'):
                    inset.set_xlim(*kwargs.get('xlim'))
                else:
                    inset.set_xlim(-750, 750)
                if self.meas[nr].info['Structure'] == 'Crosses' and kwargs.get(
                        'crosses_xlim'):
                    inset.set_xlim(*kwargs.get('crosses_xlim'))

        plt.savefig("%s.pdf" % filename)

    def plot_hloops_90(self,
                       to_show=[57, 155],
                       filename='hloop-compare-90',
                       **kwargs):
        """Compares +90deg with -90deg for (180deg Comparison)

        Args:
            to_show (TYPE, optional): DESCRIPTION. The default is [57,155].
            filename (TYPE, optional): DESCRIPTION. The default is
                'hloop-compare-90'.
            **kwargs (TYPE): mirror: bool, optional
                    should we mirror the hloop? The default is False

        Returns:
            None.:
        """
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks',
                               palette='deep', latex=True)
        sns.set_palette(sns.color_palette("deep"))

        fig, axes = plt.subplots(2, 2, figsize=(16, 12),
                                 **kwargs.get('plot_args', dict()))
        for i, nr in enumerate(to_show):
            cur_ax = axes[i]
            self.m.style.set_style(size='talk', style='ticks',
                                   palette='Paired',
                                   grid=True, latex=True)

            self.meas[nr] = Hloop(nr)
            self.set_plot_style(self.meas[nr])

            if not (kwargs.get('mirror')) and nr == 57:
                self.meas[nr].set_factor(-1)
                self.meas[nr].factor = 1

            # Plusses
            ax = cur_ax[0]
            self.meas[nr].plot_strayfield(ax, nolegend=True,
                                          show_plusses=False,
                                          show_crosses=False)

            ax.plot(self.meas[nr].up_fitted.B, self.meas[nr].up_fitted.Bx8,
                    label='Up (fitted)')
            ax.plot(self.meas[nr].down_fitted.B, self.meas[nr].down_fitted.Bx8,
                    label='Down (fitted)')

            # Set Limits (x and y Axis)
            if kwargs.get('xlim'):
                ax.set_xlim(*kwargs.get('xlim'))
            else:
                ax.set_xlim(-750, 750)

            ax.set_ylim(np.min([self.meas[nr].up_fitted.Bx8.min(),
                                self.meas[nr].down_fitted.Bx8.min()]) - .05,
                        np.max([self.meas[nr].up_fitted.Bx8.max(),
                                self.meas[nr].down_fitted.Bx8.max()]) + .05)

            ax.set_title('')
            """if nr == 57:
                ax.set_title('m57: Plusses ($90^\circ$)')
            else:
                ax.set_title('m155: Plusses ($-90^\circ$)')"""

            # Inset with Difference
            with sns.color_palette('bright'):
                if nr == 57 and not (kwargs.get('mirror')):
                    bbox = (.11, .59, .34, .35)
                else:
                    bbox = (.12, .12, .34, .35)
                inset = inset_axes(ax, width='100%', height='100%',
                                   bbox_to_anchor=bbox,
                                   bbox_transform=ax.transAxes)
                max_b = self.meas[nr].up.B.max()
                B_ext, B_stray = self.meas[nr].get_downminusup_strayfield(
                    fitted_data=True)
                inset.plot([-max_b, max_b], [0, 0], '--', color='orange',
                           linewidth=.75)
                inset.plot(B_ext, B_stray, color='black')
                inset.set_xlim(-750, 750)
                inset.set_title("Difference (fitted)")
                inset.set_yticks(kwargs.get('diff_yticks', [0, 0.5]))

            ax.legend(loc='upper right')  # , ncol=2)

            # Crosses
            ax = cur_ax[1]
            self.meas[nr].plot_strayfield(ax, nolegend=True,
                                          show_plusses=False,
                                          show_crosses=False)
            ax.plot(self.meas[nr].up_fitted.B,
                    self.meas[nr].up_fitted.Bx9, label='Up (fitted)')
            ax.plot(self.meas[nr].down_fitted.B,
                    self.meas[nr].down_fitted.Bx9, label='Down (fitted)')

            # Set Limits (x and y Axis)
            if kwargs.get('xlim'):
                ax.set_xlim(*kwargs.get('xlim'))
            else:
                ax.set_xlim(-750, 750)

            ax.set_ylim(np.min([self.meas[nr].up_fitted.Bx9.min(),
                                self.meas[nr].down_fitted.Bx9.min()]) - .05,
                        np.max([self.meas[nr].up_fitted.Bx9.max(),
                                self.meas[nr].down_fitted.Bx9.max()]) + .05)

            ax.set_title('')
            """if nr == 57:
                ax.set_title('M57: Crosses ($90^\circ$)')
            else:
                ax.set_title('M155: Crosses ($-90^\circ$)')"""

            # Inset with Difference
            with sns.color_palette('bright'):
                if nr == 57 and kwargs.get('mirror'):
                    bbox = (.12, .12, .34, .35)
                else:
                    bbox = (.12, .59, .34, .35)
                inset2 = inset_axes(ax, width='100%', height='100%',
                                    bbox_to_anchor=bbox,
                                    bbox_transform=ax.transAxes)
                f_up = scipy.interpolate.interp1d(self.meas[nr].up_fitted.B,
                                                  self.meas[nr].up_fitted.Bx9)
                f_down = scipy.interpolate.interp1d(
                    self.meas[nr].down_fitted.B,
                    self.meas[nr].down_fitted.Bx9)
                B = np.linspace(self.meas[nr].up.B.min(),
                                self.meas[nr].up.B.max(), int(1e5))
                downminusup = f_down(B) - f_up(B)
                inset2.plot([-max_b, max_b], [0, 0], '--',
                            color='orange', linewidth=.75)
                inset2.plot(B, downminusup, color='black')
                inset2.set_xlim(-750, 750)
                inset2.set_title("Difference (fitted)")
                if nr == 57:
                    inset.set_yticks([0, .25, 0.5])
                    inset2.set_yticks([0, .25, 0.5])

            ax.legend(loc='upper right')  # , ncol=2)

        for ax, struct in zip(axes[0],
                                  ['Plusses',
                                   'Crosses']):
            ax.set_title(struct,
                         fontdict={'fontsize': 32,
                                   'fontweight': 'bold'})
            ax.set_xlabel('')

        for ax in axes[:, 1]:
            ax.set_ylabel('')

        for ax, a in zip(axes[:, 0],
                         [r'{$\mathbf{\theta = +90^{\circ}}$}',
                          r'{$\mathbf{\theta = -90^{\circ}}$}']):
            ax.set_ylabel("%s\n$\\langle B_z \\rangle [\\mathrm{mT}]$" % a,
                          fontdict={'fontsize': 24,
                                    'fontweight': 'bold'})

        plt.savefig("%s.pdf" % filename)

    def plot_hloops_90_nofit(self, to_show=[57, 155],
                             filename='hloop-compare-90', **kwargs):
        """Compares +90deg with -90deg for (180deg Comparison) Using not fitted
        data

        Args:
            to_show (TYPE, optional): DESCRIPTION. The default is [57,155].
            filename (TYPE, optional): DESCRIPTION. The default is
                'hloop-compare-90'.
            **kwargs (TYPE): mirror: bool, optional
                    should we mirror the hloop? The default is False

        Returns:
            None.:
        """
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks',
                               palette='deep', latex=True)
        sns.set_palette(sns.color_palette("deep"))

        fig, axes = plt.subplots(2, 2, figsize=(16, 12))
        for i, nr in enumerate(to_show):
            cur_ax = axes[i]
            self.m.style.set_style(size='talk', style='ticks',
                                   palette='Paired',
                                   grid=True, latex=True)

            self.meas[nr] = Hloop(nr)

            if not (kwargs.get('mirror')) and nr == 57:
                self.meas[nr].set_factor(-1)
                self.meas[nr].factor = 1

            # Plusses
            ax = cur_ax[0]
            self.meas[nr].plot_strayfield(ax, nolegend=True,
                                          show_plusses=False,
                                          show_crosses=False)

            ax.plot(self.meas[nr].up.B, self.meas[nr].up.Bx8, label='Up')
            ax.plot(self.meas[nr].down.B, self.meas[nr].down.Bx8, label='Down')
            ax.plot(self.meas[nr].up.B, self.meas[nr].up.Bx13,
                    label='Up (Empty)')
            ax.plot(self.meas[nr].down.B, self.meas[nr].down.Bx13,
                    label='Down (Empty)')

            # Set Limits (x and y Axis)
            if kwargs.get('xlim'):
                ax.set_xlim(*kwargs.get('xlim'))
            else:
                ax.set_xlim(-1000, 1000)

            ax.set_ylim(np.min([self.meas[nr].up.Bx8.min(),
                                self.meas[nr].down.Bx8.min()]) - .05,
                        np.max([self.meas[nr].up.Bx8.max(),
                                self.meas[nr].down.Bx8.max()]) + .05)

            if nr == 57:
                ax.set_title('M57: Plusses ($90^\circ$)')
            else:
                ax.set_title('M155: Plusses ($-90^\circ$)')

            # Inset with Difference
            with sns.color_palette('bright'):
                if nr == 57 and not (kwargs.get('mirror')):
                    bbox = (.11, .59, .34, .35)
                else:
                    bbox = (.59, .12, .34, .35)
                inset = inset_axes(ax, width='100%', height='100%',
                                   bbox_to_anchor=bbox,
                                   bbox_transform=ax.transAxes)
                max_b = self.meas[nr].up.B.max()
                B_ext, B_stray = self.meas[nr].get_downminusup_strayfield()
                inset.plot([-max_b, max_b], [0, 0], '--', color='orange',
                           linewidth=.75)
                inset.plot(B_ext, B_stray)
                inset.set_xlim(-1000, 1000)
                inset.set_title("Difference")

            # Crosses
            ax = cur_ax[1]
            self.meas[nr].plot_strayfield(ax, nolegend=True,
                                          show_plusses=False,
                                          show_crosses=False)
            ax.plot(self.meas[nr].up.B, self.meas[nr].up.Bx9, label='Up')
            ax.plot(self.meas[nr].down.B, self.meas[nr].down.Bx9, label='Down')
            ax.plot(self.meas[nr].up.B, self.meas[nr].up.Bx13,
                    label='Up (Empty)')
            ax.plot(self.meas[nr].down.B, self.meas[nr].down.Bx13,
                    label='Down (Empty)')

            # Set Limits (x and y Axis)
            if kwargs.get('xlim'):
                ax.set_xlim(*kwargs.get('xlim'))
            else:
                ax.set_xlim(-1000, 1000)

            ax.set_ylim(np.min([self.meas[nr].up.Bx9.min(),
                                self.meas[nr].down.Bx9.min()]) - .05,
                        np.max([self.meas[nr].up.Bx9.max(),
                                self.meas[nr].down.Bx9.max()]) + .05)

            if nr == 57:
                ax.set_title('M57: Crosses ($90^\circ$)')
            else:
                ax.set_title('M155: Crosses ($-90^\circ$)')

            # Inset with Difference
            with sns.color_palette('bright'):
                if nr == 57 and kwargs.get('mirror'):
                    bbox = (.11, .12, .34, .35)
                else:
                    bbox = (.11, .59, .34, .35)
                inset2 = inset_axes(ax, width='100%', height='100%',
                                    bbox_to_anchor=bbox,
                                    bbox_transform=ax.transAxes)
                f_up = scipy.interpolate.interp1d(self.meas[nr].up.B,
                                                  self.meas[nr].up.Bx9)
                f_down = scipy.interpolate.interp1d(self.meas[nr].down.B,
                                                    self.meas[nr].down.Bx9)
                B = np.linspace(self.meas[nr].up.B.min(),
                                self.meas[nr].up.B.max(), int(1e5))
                downminusup = f_down(B) - f_up(B)
                inset2.plot([-max_b, max_b], [0, 0], '--',
                            color='orange', linewidth=.75)
                inset2.plot(B, downminusup, color='green')
                inset2.set_xlim(-1000, 1000)
                inset2.set_title("Difference")
                if nr == 57:
                    inset.set_yticklabels(['', '$0.0$', '', '$0.5$'])
                    inset2.set_yticklabels(['', '$0.0$', '', '$0.5$'])

            # if nr == 57 and not kwargs.get('mirror'):
            #    ax.legend(loc='upper left')#, ncol=2)
            if not (nr == 57):
                ax.legend(loc='upper right')  # , ncol=2)

        plt.savefig("%s.pdf" % filename)

    def plot_hloops2(self, to_show=[58, 59, 61, 62],
                     filename='hloop-compare-3', **kwargs):
        """Outputs hloop-compare-3.pdf (not used anymore)

        Args:
            to_show (TYPE, optional): DESCRIPTION. The default is a[85]+a[100].
            filename (TYPE, optional): DESCRIPTION. The default is
                'hloop-compare-3'.
            **kwargs (TYPE): DESCRIPTION.

        Returns:
            None.:
        """
        fig, axes = plt.subplots(2, 2, figsize=(16, 12))
        for i, nr in enumerate(to_show):
            ax = axes[i // 2][i % 2]
            self.m.style.set_style(size='talk', style='ticks',
                                   grid=True, latex=True)
            self.meas[nr] = Hloop(nr)
            self.meas[nr].plot_strayfield(ax)
            ax.set_xlim(-750, 750)
            if nr == 58:
                ax.set_ylim(self.meas[nr].down_fitted.Bx8.max(),
                            self.meas[nr].up_fitted.Bx8.min())
            with sns.color_palette('deep'):
                self.m.style.set_style(size='talk', style='ticks',
                                       grid=True, latex=True)
                if nr in [58, 59]:
                    bbox = (.13, .02, .35, .35)
                else:
                    bbox = (.65, .02, .35, .35)
                inset = inset_axes(ax, width='100%', height='90%',
                                   bbox_to_anchor=bbox,
                                   bbox_transform=ax.transAxes)
                max_b = self.meas[nr].up.B.max()
                inset.plot([-max_b, max_b], [0, 0], 'r--', linewidth=.75)
                B_ext, B_stray = self.meas[nr].get_downminusup_strayfield()
                inset.plot(B_ext, B_stray)
                inset.set_xlim(-750, 750)
                if not (nr in [58, 59]):
                    inset.set_ylabel("$\Delta B_z\\;[\\mathrm{mT}]$")
                    inset.set_title("Difference")
                inset.set_xticklabels([])
                inset.set_xticks([])

        plt.savefig("%s.pdf" % filename)

    def plot_hloops3(self, to_show=[139, 140],
                     filename='hloop-repeat',
                     xlim=(-350, 350),
                     ylim=None,
                     inset_xlim=(-30, 30),
                     inset_ylim=(0.15, 1.25)
                     ):
        """
            Compares two measurements at the same Angle (repeated
            measurements).

        Args:
            to_show (TYPE, optional): DESCRIPTION. The default is [139,140].
            filename (TYPE, optional): DESCRIPTION. The default is
                'hloop-repeat'.
            xlim (TYPE, optional): DESCRIPTION. The default is (-350, 350).
            ylim:
            inset_xlim (TYPE, optional): DESCRIPTION. The default is (-30, 30).
            inset_ylim (TYPE, optional): DESCRIPTION. The default is (.15,
                1.25).

        Returns:
            None.:
        """
        self.m.style.set_style(size='talk', style='ticks', palette='Paired',
                               grid=True, latex=True)
        fig, ax = plt.subplots(1, 1, figsize=(16, 12))
        bmin, bmax = [], []
        for i, nr in enumerate(to_show):
            # ax = axes[i//2][i%2]
            self.meas[nr] = Hloop(nr)
            self.meas[nr].plot_strayfield(ax, nolegend=True)
            ax.set_xlim(*xlim)
            bmin1, bmax1 = self.meas[nr].get_bhminmax()
            bmin.append(bmin1)
            bmax.append(bmax1)

        if ylim:
            ax.set_ylim(*ylim)
        else:
            ax.set_ylim(np.min(bmin) - .05, np.max(bmax) + .05)
        ax.set_title("M%s/%s: %s ($%s^\\circ$)" % (to_show[0], to_show[1],
                                                   self.meas[nr].info[
                                                       'Structure'].replace(
                                                       '_', r'\_'),
                                                   self.meas[nr].info[
                                                       'Angle']))
        ax.legend(["M%d: Up" % to_show[0], "M%d: Down" % to_show[0],
                   "M%d: Up" % to_show[1], "M%d: Down" % to_show[1], ])
        y1, y2 = inset_ylim[0], inset_ylim[1]
        x1, x2 = inset_xlim[0], inset_xlim[1]
        ax.fill([x1, x1, x2, x2], [y1, y2, y2, y1], 'blue', alpha=.1)

        with sns.color_palette('bright'):
            bbox = (.65, .055, .35, .3)
            inset = inset_axes(ax, width='100%', height='100%',
                               bbox_to_anchor=bbox,
                               bbox_transform=ax.transAxes)
            max_b = self.meas[nr].up.B.max()
            for nr in to_show:
                B_ext, B_stray = self.meas[nr].get_downminusup_strayfield()
                inset.plot(B_ext, B_stray)
                inset.plot([-max_b, max_b], [0, 0], '--', linewidth=.75)
            inset.set_xlim(*xlim)
            inset.set_ylabel("$\Delta B_z\\;[\\mathrm{mT}]$")
            inset.set_title("Difference")

        with sns.color_palette('Paired'):
            bbox = (.07, .45, .35, .35)
            inset2 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=bbox,
                                bbox_transform=ax.transAxes)
            for nr in to_show:
                self.meas[nr].plot_strayfield(inset2, nolegend=True)
            inset2.set_xlim(*inset_xlim)
            inset2.set_ylim(*inset_ylim)
            inset2.set_title("")
            inset2.set_ylabel('')

        with sns.color_palette('Paired'):
            bbox = (.65, .43, .35, .3)
            inset3 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=bbox,
                                bbox_transform=ax.transAxes)
            self.plot_hloop_gradient3(inset3, nr=to_show,
                                      limit=inset_xlim[1], )
            # inset3.set_xticklabels([''])
            inset3.set_xlim(*inset_xlim)
            inset3.set_xlabel('')
            inset3.legend_.remove()

        plt.savefig("%s.pdf" % filename)

    def plot_hloops_compare_90(self, to_show=[414, 415],
                               structure='plusses',
                               filename='hloop-repeat-414',
                               xlim=(-750, 750),
                               ylim=(-.36, .76),
                               inset_xlim=(-225, 225),
                               inset_ylim=(-.35, .75),
                               insets=[((.05, .05, .35, .35), ((-225, 225), (-.35, .75), 'blue', .1))],
                               legend_loc='upper right',
                               nograd=False,
                               nodiff=False,
                               figsize=(16, 12),
                               ):
        """
            Compares multiple measurements at 90 deg (repeated
            measurements).

        Args:
            to_show (TYPE, optional): DESCRIPTION. The default is [414,415].
            structure:
            filename (TYPE, optional): DESCRIPTION. The default is
                'hloop-repeat-414'.
            xlim (TYPE, optional): DESCRIPTION. The default is (-750, 750).
            ylim:
            inset_xlim (TYPE, optional): DESCRIPTION. The default is (-100,
                100).
            inset_ylim (TYPE, optional): DESCRIPTION. The default is (-1.25,
                .75).
            legend_loc:
            nograd:

        Returns:
            None.:
        """
        self.m.style.set_style(size='talk', style='ticks',
                               palette='Paired',
                               grid=True, latex=True)
        fig, ax = plt.subplots(1, 1, figsize=figsize)
        legend = []
        if structure == 'plusses':
            column = 'Bx8'
        elif structure == 'crosses':
            column = 'Bx9'

        for i, nr in enumerate(to_show):
            # ax = axes[i//2][i%2]
            self.meas[nr] = Hloop(nr)  # Load measurement
            self.meas[nr].remove_zero()  # Remove measurement Errors

            self.meas[nr].plot_strayfield(ax, nolegend=True,
                                          show_plusses=(
                                                  structure == 'plusses'),
                                          show_crosses=(
                                                  structure == 'crosses'))
            ax.set_xlim(*xlim)

            legend += ["m%d: Up" % nr, "m%d: Down" % nr]

        m_numbers = '/'.join(map(str, to_show))

        if ylim:
            ax.set_ylim(*ylim)

        self.m.style.set_style(size='talk', style='ticks', palette='Paired',
                               grid=True, latex=True)
        ax.set_title("\\textbf{%s} ($%s^{\\circ}$)" % (
                                                structure.capitalize(),
                                                self.meas[nr].info['Angle']))

        self.m.style.set_style(size='notebook', style='ticks',
                               palette='Paired',
                               grid=True, latex=True)
        all_insets = []
        if not nodiff:
            with sns.color_palette('bright'):
                bbox = (.7, .06, .3, .3)
                inset = inset_axes(ax, width='100%', height='100%',
                                   bbox_to_anchor=bbox,
                                   bbox_transform=ax.transAxes)
                all_insets.append(inset)
                max_b = self.meas[nr].up.B.max()
                for i, nr in enumerate(to_show):
                    if structure == 'plusses':
                        B_ext, B_stray = self.meas[nr].get_downminusup_strayfield(
                            fitted_data=True)
                    else:
                        up = self.meas[nr].up_fitted
                        down = self.meas[nr].down_fitted
                        f_up = scipy.interpolate.interp1d(up.B, up.Bx9)
                        f_down = scipy.interpolate.interp1d(down.B, down.Bx9)
                        B_ext = np.linspace(up.B.min(), up.B.max(), int(1e5))
                        B_stray = f_down(B_ext) - f_up(B_ext)
                    if i == 3:
                        inset.plot(B_ext, B_stray, color='orange')
                    else:
                        inset.plot(B_ext, B_stray)
                    if i == 0:
                        inset.plot([-max_b, max_b], [0, 0], '--', linewidth=.75)

                inset.set_xlim(*xlim)
                inset.set_ylabel("$\Delta B_z\\;[\\mathrm{mT}]$")
                inset.set_title("Difference")

        subfig = ['d', 'b', 'a', 'c']
        for bbox, (inset_xlim, inset_ylim, color, alpha) in insets:
            with sns.color_palette('Paired'):
                inset2 = inset_axes(ax, width='100%', height='100%',
                                    bbox_to_anchor=bbox,
                                    bbox_transform=ax.transAxes)
                all_insets.append(inset2)
                for i, nr in enumerate(to_show):
                    inset2.plot(self.meas[nr].up_fitted.B,
                                self.meas[nr].up_fitted[column])
                    inset2.plot(self.meas[nr].down_fitted.B,
                                self.meas[nr].down_fitted[column])
                inset2.set_xlim(*inset_xlim)
                inset2.set_ylim(*inset_ylim)

            (ann_x, ann_xx), (ann_yy, ann_y) = inset2.get_xlim(), inset2.get_ylim()
            ann_x += (ann_xx - ann_x) * .05
            ann_y -= (ann_y - ann_yy) * .20
            inset2.text(x=ann_x, y=ann_y, s=subfig.pop(),
                          fontdict=dict(fontweight='bold', fontsize=24),
                          bbox=dict(boxstyle="round,pad=0.1", fc='#ccf', ec="b", lw=1))

            # Draw Area into big plot
            y1, y2 = inset_ylim[0], inset_ylim[1]
            x1, x2 = inset_xlim[0], inset_xlim[1]
            coords = [x1, x1, x2, x2], [y1, y2, y2, y1]
            inset2.fill(*coords, color, alpha=alpha)
            ax.fill(*coords, color, alpha=alpha)

        if not nograd:
            with sns.color_palette('Paired'):
                bbox = (.08, .67, .27, .3)
                inset3 = inset_axes(ax, width='100%', height='100%',
                                    bbox_to_anchor=bbox,
                                    bbox_transform=ax.transAxes)
                all_insets.append(inset3)
                c = column.replace('B', 'V')
                self.plot_hloop_gradient3(inset3, to_show, limit=inset_xlim[1],
                                          column=c)
                inset3.set_xlim(*inset_xlim)
                inset3.set_xlabel('')
                inset3.legend_.remove()

        sns.set('notebook')
        ax.legend(legend, loc=legend_loc, ncol=int(len(to_show) / 2))

        plt.savefig("%s.png" % filename)

        return fig, (ax, all_insets)

    def plot_hloops4(self, filename='hloop-parallel',
                     figtitle="M57: $90^\\circ$ Parallel measurement",
                     **kwargs):
        """Plots a single measurement (default: 90deg parallel)

        Args:
            filename (TYPE, optional): DESCRIPTION. The default is
                'hloop-parallel'.
            figtitle (TYPE, optional): DESCRIPTION. The default is "M57:
                $90^\circ$ Parallel measurement".
            **kwargs (TYPE): DESCRIPTION.

        Returns:
            None.:
        """
        fig, ax = plt.subplots(1, 1, figsize=(16, 12))
        self.m.style.set_style(size='talk', style='ticks', palette='Paired',
                               grid=True, latex=True)
        # self.mset_factor(-1)
        # self.mfactor = 1

        self.m.plot_strayfield(ax, figtitle=figtitle,
                               show_crosses=False)
        self.m.up_fitted.Bx9 += .25
        self.m.down_fitted.Bx9 += .25
        ax.plot(self.m.up_fitted.B, self.m.up_fitted.Bx9,
                label='Crosses: Up (fitted)')
        ax.plot(self.m.down_fitted.B, self.m.down_fitted.Bx9,
                label='Crosses: Down (fitted)')
        ax.set_xlim(-750, 750)
        if kwargs.get('ylim'):
            ax.set_ylim(*kwargs.get('ylim'))
        else:
            ax.set_ylim(-.8, 1.8)
        ax.legend(loc='best')

        with sns.color_palette('bright'):
            bbox = (.06, .46, .35, .23)
            inset = inset_axes(ax, width='100%', height='100%',
                               bbox_to_anchor=bbox,
                               bbox_transform=ax.transAxes)
            max_b = self.m.up.B.max()
            B_ext, B_stray = self.m.get_downminusup_strayfield()
            inset.plot([-max_b, max_b], [0, 0], '--', color='orange',
                       linewidth=.75)
            inset.plot(B_ext, B_stray)
            inset.set_xlim(-750, 750)
            inset.set_title("Difference Plusses")

            bbox = (.06, .06, .35, .22)
            inset2 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=bbox,
                                bbox_transform=ax.transAxes)
            f_up = scipy.interpolate.interp1d(self.m.up.B, self.m.up.Bx9)
            f_down = scipy.interpolate.interp1d(self.m.down.B, self.m.down.Bx9)
            B = np.linspace(self.m.up.B.min(), self.m.up.B.max(), int(1e5))
            downminusup = f_down(B) - f_up(B)
            inset2.plot([-max_b, max_b], [0, 0], '--', color='orange',
                        linewidth=.75)
            inset2.plot(B, downminusup, color='green')
            inset2.set_xlim(-750, 750)
            inset2.set_title("Difference Crosses")

        plt.savefig("%s.pdf" % filename)

    def plot_cubes_trees(self):
        """Plots difference between RAW data from Cubes and Trees (1st
        Generation)

        Returns:
            None.:
        """
        self.m.style.set_style(size='talk', style='ticks', palette='deep',
                               grid=True, latex=True)
        file_list = ['data/Data/%sdeg_%s' % (angle, struct) for
                     angle in [90, -90] for
                     struct in ['cubes', 'Trees']
                     ]

        fig, axes = plt.subplots(2, 2, figsize=(16, 12))
        comp = {}
        for i, f in enumerate(file_list):
            ax = axes[i // 2][i % 2]
            load_files = ['%s_%s.dat' % (f, direction) for
                          direction in ['a', 'b']]
            comp[i] = Hloop(0, file_list=load_files)
            comp[i].set_factor(1e6)

            ax.plot(comp[i].up.B, comp[i].up.Vx8, label='Up')
            ax.plot(comp[i].down.B, comp[i].down.Vx8, label='Down')

            ax.set_ylabel("$V_x [\\mathrm{\\mu V}]$")
            ax.set_xlabel("$\\mu_0 H_{ext}$ $[\\mathrm{mT}]$")
            bmin, bmax, vmin, vmax = comp[i].get_minmax()
            ax.set_xlim(bmin, bmax)

            if f.find('cubes') > 0:
                struct = 'Cubes'
            else:
                struct = "Trees"

            ax.set_title("%s ($%s^\\circ$)" % (struct, f[10:13].strip('d')))

            with sns.color_palette('bright'):
                if i % 2 == 1:
                    bbox = (.69, .7, .3, .3)
                else:
                    bbox = (.12, .7, .29, .3)
                inset = inset_axes(ax, width='100%', height='100%',
                                   bbox_to_anchor=bbox,
                                   bbox_transform=ax.transAxes)
                max_b = self.m.up.B.max()
                B_ext, B_stray = comp[i].get_downminusup()
                inset.plot([-max_b, max_b], [0, 0], '--', color='orange',
                           linewidth=.75)
                inset.plot(B_ext, B_stray)
                inset.set_xlim(bmin, bmax)
                # inset.set_title("Difference")

        ax.legend(loc='lower left')

        plt.savefig("compare-cubes-trees.pdf")

    def plot_hloop_gradient(self, limit=50, filename='hloop-gradient'):
        """Plotting the gradient along the

        Args:
            limit:
            filename:

        Returns:
            None.:
        """
        self.m.style.set_style(default=True, grid=True,
                               style='ticks',
                               size='talk',
                               palette='deep',
                               latex=True, )

        grad1 = [np.divide(np.diff(self.m.up.Vx8), np.diff(self.m.up.Time)),
                 np.divide(np.diff(self.m.up.Vx9), np.diff(self.m.up.Time))]
        grad12 = [
            np.divide(np.diff(self.m.down.Vx8), np.diff(self.m.down.Time)),
            np.divide(np.diff(self.m.down.Vx9), np.diff(self.m.down.Time))]

        fig, axes = plt.subplots(2, 1, sharex=True)
        for i in range(2):
            ax = axes[i]
            g = grad1[i]
            g2 = grad12[i]

            if i == 0:
                add = 'Plusses'
            else:
                add = 'Crosses'

            ax.set_title('M57: Gradient (%s)' % add)
            x = self.m.up[:-1]
            ax.plot(x.B[x.B.abs() < limit],
                    g[x.B.abs() < limit] * 1e6,
                    label='Up (%s)' % add)
            x = self.m.down[:-1]
            ax.plot(x.B[x.B.abs() < limit],
                    g2[x.B.abs() < limit] * 1e6,
                    label='Down (%s)' % add)
            ax.set_ylabel('$dV_H/dt\; [\\mu\\mathrm{V/s}]$')
        ax.set_xlabel('$\\mu_0 H_{ext} [\\mathrm{mT}]$')

        plt.savefig('%s.pdf' % filename)

    def plot_hloop_gradient2(self, limit=50, filename='hloop-gradient'):
        """Plotting the gradient along the

        Args:
            limit:
            filename:

        Returns:
            None.:
        """
        self.m.style.set_style(default=True, grid=True,
                               style='ticks',
                               size='talk',
                               palette='deep',
                               latex=True, )

        grad = [np.gradient(self.m.up.Vx8, np.diff(self.m.up.Time).mean()),
                np.gradient(self.m.down.Vx8, np.diff(self.m.down.Time).mean())]

        fig, ax = plt.subplots()
        ax.set_title('M%s: Gradient (%s)' % (self.m.self.measurement_number,
                                             self.m.info['Structure']))
        for i in range(2):
            g = grad[i]

            if i == 0:
                x = self.m.up
                add = 'Up'
            else:
                x = self.m.down
                add = 'Down'

            ax.plot(x.B[x.B.abs() < limit],
                    g[x.B.abs() < limit] * 1e6,
                    label='%s' % add)
        ax.set_ylabel('$dV_H/dt\; [\\mu\\mathrm{V/s}]$')
        ax.legend(loc='best')
        ax.set_xlabel('$\\mu_0 H_{ext} [\\mathrm{mT}]$')

        plt.savefig('%s.pdf' % filename)

    def plot_hloop_gradient3(self, ax, to_show=[139, 140], limit=50,
                             column='Vx8'):
        """Plotting the gradient along the

        Args:
            ax:
            to_show:
            limit:
            column:

        Returns:
            None.:
        """
        # self.m.style.set_style(default=True, grid=True,
        #        style='ticks',
        #        size='talk',
        #        palette='Paired',
        #        latex=True,)

        limit = np.abs(limit)

        ax.set_title('Gradient')
        for nr in to_show:
            self.m = self.meas[nr]
            c = column
            grad = [np.divide(np.diff(self.m.up[c]), np.diff(self.m.up.Time)),
                    np.divide(np.diff(self.m.down[c]),
                              np.diff(self.m.down.Time))]
            for i in range(2):
                g = grad[i]

                if i == 0:
                    x = self.m.up[:-1]
                    direction = 'Up'
                else:
                    x = self.m.down[:-1]
                    direction = 'Down'

                ax.plot(x.B[x.B.abs() < limit],
                        g[x.B.abs() < limit] * 1e6,
                        label='M%s: %s' % (nr, direction))

        # ax.set_xlim(-limit, limit)
        ax.set_ylabel('$dV_H/dt\; [\\mu\\mathrm{V/s}]$')
        ax.legend(loc='best')
        ax.set_xlabel('$\\mu_0 H_{ext} [\\mathrm{mT}]$')

    def plot_hloop_temp(self):
        """Plots the Temperature of a Hloop measurement.

        Returns:
            None.:
        """

        self.m.style.set_style(default=True, grid=True,
                               style='ticks',
                               size='talk',
                               palette='deep',
                               latex=True, )

        fig, ax = plt.subplots()
        ax.set_title(
            'M%s: Sample Temperature' % self.m[self.measurement_number])

        ax.plot(self.m.up.B, (self.m.up['Sample Temp']), '-', label='Up')
        ax.plot(self.m.down.B, (self.m.down['Sample Temp']), '-', label='Down')

        ax.set_xlabel('$\\mu_0 H_{ext} [\\mathrm{mT}]$')
        ax.set_ylabel('Temperature $[\\mathrm{mK}]$')
        ax.legend(loc='best')

        plt.savefig('hloop-90-temp.pdf')

    #### Plot Static Fields
    def plot_static_fields(self, filename='static-field',
                           show_inset=False):
        """Plot the Signal Analyzer (SA) Data for static fields (not sweeping)

        Returns:
            None.:
        """
        tmp = {
            'Plusses (25 mT)': 360,
            'Plusses (-25 mT)': 361,
            'Plusses (-9.4 mT)': 282,
            'Crosses (-9.4 mT)': 283,
            'Plusses (0 T)': 281,
            'Crosses (0 T)': 280,
            'Empty (0 T)': 310,
            # 'Crosses (250 mT)': 315,
        }
        lofm = {}
        for i, j in tmp.items():
            lofm.update({j: ['%s' % i, {}]})

        self.eva.style.set_style(default=True, grid=True,
                                 style='ticks',
                                 size='talk',
                                 palette='Paired',
                                 latex=True, )

        fig, ax = self.eva.plot(lofm,
                                plot_settings=dict(
                                    title='($90^\\circ$) Static Field',
                                    xlim=(2e-2, 5e0),
                                    ylim=(1e-7, 1e-3),
                                ),
                                f_settings=dict(
                                    ymin=5e-5),
                                f2_settings=dict(disable=True),
                                )

        # ax = plt.gca()
        if show_inset:
            with sns.color_palette('deep'):
                inset = inset_axes(ax, width='100%', height='100%',
                                   bbox_to_anchor=(.25, .58, .45, .4),
                                   bbox_transform=ax.transAxes)
                self.m.style.set_style(default=True, grid=True,
                                       style='ticks',
                                       size='talk',
                                       palette='Paired',
                                       latex=True)
                self.m.plot_strayfield(inset, 'Strayfield Plusses', nolegend=True)
                inset.legend(['Up',  # ' ($-M_S \\rightarrow +M_S$)',
                              'Down'])  # ' ($+M_S \\rightarrow -M_S$)'])
                inset.grid(visible=True, alpha=.4)
                inset.set_xlim(-50, 50)
                inset.set_ylim(-.65, .45)

                def a(x):
                    return self.m.down.iloc[
                        ((self.m.down.B - x) ** 2).idxmin()]['Bx8']

                def b(x):
                    return self.m.up.iloc[((self.m.up.B - x) ** 2).idxmin()]['Bx8']

                inset.plot((-25, -25), (a(-25), a(-25)), 'o', color='#000099',
                           alpha=.6)
                inset.plot([25, 25], [b(25), b(25)], 'o', color='#9999FF',
                           alpha=.6)
                inset.plot([-9.4, -9.4], [a(-9.4), a(-9.4)], 'o', color='green',
                           alpha=.6)
                inset.plot([0], [a(0)], 'o', color='#FF3333', alpha=.8)

                # ana.set_relative_yticks(inset)
                inset.set_xticks(np.arange(-50, 51, 25))

        plt.savefig('{}.pdf'.format(filename))

    #### Sweeping Field
    def first_sweeps(self, add=False, filename='vacant-sweeps',
                     show_inset=False):
        """First field sweeps (plus minus 50/75/100/etc.)

        Args:
            add: adding more data
            filename: saving as pdf (default: vacant-sweeps)

        Returns:
            vacant-sweeps.pdf:
        """
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True,
                               palette='deep')

        lofm = {}
        to_show = {
            # 382: [(-25,25), 'Plusses', 5, {}],
            362: [(-50, 50), 'Plusses', 2, {}],
            363: [(-75, 75), 'Plusses', 2, {}],
            364: [(-100, 100), 'Plusses', 2, {}],
            366: [(-125, 125), 'Plusses', 2, {}],
            365: [(-150, 150), 'Plusses', 2, {}],
            # 352: [("-M_s \\rightarrow -25",25), 'Plusses', .1],
        }
        if add:
            to_show.update({
                336: [(-500, 500), 'Plusses', 9.4, {}],
                331: [('\\mathrm{C}\\,{%s}' % -100, 100), 'Crosses', 9.4,
                      {'linestyle': '--'}],
                332: [('\\mathrm{C}\\,{%s}' % -300, 300), 'Crosses', 9.4,
                      {'linestyle': '--'}],
                333: [('\\mathrm{C}\\,{%s}' % -750, 750), 'Crosses', 9.4,
                      {'linestyle': '--'}],
            })

        for nr, content in to_show.items():
            lofm[nr] = ["$%s \\rightarrow %s\\;\\mathrm{mT}$" % (
                content[0][0],
                content[0][1],
            ), content[3]]

        fig, ax = self.eva.plot(lofm,
                                # fit_range=(2e-2, 9e-1),
                                # show_fit=True,
                                plot_settings=dict(
                                    title='\\Huge \\textbf{b) Sweeping Inside the Hysteresis (Plusses)}',
                                    fontdict={'fontsize': 64,
                                              'fontweight': 'bold'},
                                    xlim=(1e-2, 1e0),
                                    ylim=(4e-7, 7e-2)),
                                f_settings=dict(
                                    disable=True,
                                    xmin=5e-2,
                                    ymin=1e-5),
                                f2_settings=dict(
                                    xmin=2e-2,
                                    ymin=1e-2,
                                ),
                                )

        if show_inset and not add: # Inset with Strayfield
            with sns.color_palette('deep'):
                inset = inset_axes(ax, width='100%', height='100%',
                                   bbox_to_anchor=(.74, .45, .25, .25),
                                   bbox_transform=ax.transAxes)
                # Plotting Lines for each measurement
                y1, y2 = -1, 1
                for nr, content in to_show.items():
                    x1 = content[0][1]
                    inset.plot([x1, x1, -x1, -x1], [y1, y2, y2, y1])

                self.m.plot_strayfield(inset, 'Strayfield Plusses',
                                       show_plusses=False,
                                       show_crosses=False,
                                       nolegend=True)

                inset.plot(self.m.up_fitted.B, self.m.up_fitted.Bx8, '-',
                           color=(76 / 255, 114 / 255, 176 / 255),
                           linewidth=3, label='Up')
                inset.plot(self.m.down_fitted.B, self.m.down_fitted.Bx8, 'r-',
                           color=(221 / 255, 132 / 255, 82 / 255),
                           linewidth=1.5, label='Down')

                inset.legend(['Up',  # ($-M_S \\rightarrow +M_S$)',
                              'Down'])  # ($+M_S \\rightarrow -M_S$)'])
                inset.grid(visible=True, alpha=.4)
                inset.set_xlim(-200, 200)
                inset.set_ylim(-.65, .45)
                inset.set_xticks([-200 + 50 * _ for _ in range(9)])
                inset.set_xticks([-175 + 50 * _ for _ in range(8)], minor=True)
                inset.set_yticks([-0.25 + .5 * _ for _ in range(2)],
                                 minor=True)
                inset.grid(visible=True, which='minor', color='#cccccc',
                           linestyle='-.', alpha=.3)

                # X-Ticks (labels)
                xt_labels = []
                for i in [-200 + 50 * _ for _ in range(9)]:
                    if i % 100 == 0:
                        xt_labels += ['$%s$' % i]
                    else:
                        xt_labels += ['']
                inset.set_xticklabels(xt_labels)

        # Inset showing fitted data
        with sns.color_palette("deep"):
            inset2 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=(.44, .75, .3, .25),
                                bbox_transform=ax.transAxes)
            inset3 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=(.09, .09, .3, .25),
                                bbox_transform=ax.transAxes)

            i2data = {}
            for nr, content in to_show.items():
                intercept, slope = self.eva[nr].fit(fit_range=(1e-2, 2e-1))
                voltage = content[0][1]
                i2data[nr] = {'end field': voltage,
                              'alpha': -slope,
                              'amplitude': 10 ** intercept}

                inset2.plot(voltage, 10 ** intercept, 'o')
                inset3.plot(voltage, -slope, 'o')

            inset2.set_xlabel('End field [$\mu_0 \mathrm{H_{ext}}$]')
            inset2.set_ylabel('$S_{V_H} (f=1\\;$Hz$)$')
            inset2.set_yscale('log')
            inset2.set_yticks([8e-7, 9e-7, 1e-6, 2e-6])

            inset3.set_xlabel('End field [$\mu_0 \mathrm{H_{ext}}$]')
            inset3.set_ylabel('$\\alpha$')

        plt.savefig('%s.pdf' % filename)
        return i2data

    def compare_voltages(self, filename='compare-voltages',
                         show_inset=False):
        """Compare different applied Voltages.

        Returns:
            compare-voltages.pdf:
        """
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True,
                               palette='deep')

        lofm = {}
        to_show = {}
        for i in range(1, 6):
            to_show[i + 377] = i

        for nr, content in to_show.items():
            lofm[nr] = ["$%s\\;\\mu\\mathrm{A}$" % (
                content
            ), {}]

        fig, ax = self.eva.plot(lofm,
                                # fit_range=(2e-2, 7e-1),
                                # show_fit=True,
                                plot_settings=dict(
                                    title='Current Amplitudes (SR785)',
                                    xlim=(1e-2, 1e0),
                                    ylim=(4e-7, 7e-2)),
                                f_settings=dict(disable=True,
                                                xmin=5e-2,
                                                ymin=1e-5),
                                f2_settings=dict(
                                    xmin=1.5e-1,
                                    ymin=2e-3,
                                ),
                                )

        if show_inset:
            # Inset with Strayfield
            with sns.color_palette('deep'):
                inset = inset_axes(ax, width='100%', height='100%',
                                   bbox_to_anchor=(.07, .38, .26, .26),
                                   bbox_transform=ax.transAxes)
                self.m.plot_strayfield(inset, 'Strayfield Plusses',
                                       nolegend=True, )
                inset.legend(['Up',  # ($-M_S \\rightarrow +M_S$)',
                              'Down'])  # ($+M_S \\rightarrow -M_S$)'])
                inset.grid(visible=True, alpha=.4)
                inset.set_xlim(-50, 50)
                inset.set_ylim(-.65, .45)
                inset.set_xticks([-50 + 25 * _ for _ in range(5)])
                # inset.set_xticks([-45+10*_ for _ in range(10)], minor=True)
                inset.grid(visible=True, which='minor', color='#cccccc',
                           linestyle='-.', alpha=.3)
                # inset.set_xlabel('')
                inset.set_ylabel('')

                y1, y2 = -1, 2
                inset.fill([-25, -25, 25, 25], [y1, y2, y2, y1], 'blue', alpha=.1)
                inset.annotate("", xy=(25, -.35), xytext=(-25, -.2),
                               arrowprops=dict(arrowstyle="->", color='blue'))

        # Inset showing fitted data
        with sns.color_palette("deep"):
            inset2 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=(.54, .75, .3, .25),
                                bbox_transform=ax.transAxes)
            inset3 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=(.3, .09, .3, .25),
                                bbox_transform=ax.transAxes)

            i2data = {}
            for nr, content in to_show.items():
                intercept, slope = self.eva[nr].fit(fit_range=(2e-2, 7e-1))
                voltage = content
                i2data[nr] = {'voltage': voltage,
                              'alpha': -slope,
                              'amplitude': 10**intercept}

                inset2.plot(voltage, 10 ** intercept, 'o')
                inset3.plot(voltage, -slope, 'o')

            inset2.set_xlabel('Current [$\\mu\\mathrm{A}$]')
            inset2.set_ylabel('$S_{V_H} (f=1\\;$Hz$)$')
            inset2.set_yscale('log')

            inset3.set_xlabel('Current [$\\mu\\mathrm{A}$]')
            inset3.set_ylabel('$\\alpha$')

        plt.savefig('{}.pdf'.format(filename))
        return i2data

    def compare_sweeprates(self, filename='compare-sweeprates',
                           show_inset = False):
        """Compare different Sweeprates, fits the data and

        Returns:
            None.:
        """
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True,
                               palette='deep')

        lofm = {}
        to_show = {
            382: [("-M_s \\rightarrow -25", 25), 'Plusses', 5],
            354: [("-M_s \\rightarrow -25", 25), 'Plusses', 2],
            351: [("-M_s \\rightarrow -25", 25), 'Plusses', 1],
            355: [("-M_s \\rightarrow -25", 25), 'Plusses', .5],
            353: [("-M_s \\rightarrow -25", 25), 'Plusses', .25],
            352: [("-M_s \\rightarrow -25", 25), 'Plusses', .1],
        }

        for nr, content in to_show.items():
            lofm[nr] = ["$%s\\;\\frac{\\mathrm{mT}}{\\mathrm{min}}$" % (
                content[2],
            ), {}]

        fig, ax = self.eva.plot(lofm,
                                # fit_range=(2e-2, 5e-1),
                                # show_fit=True,
                                plot_settings=dict(
                                    title='Sweeprates (SR785)',
                                    xlim=(1e-2, 1.6e0),
                                    ylim=(4e-7, 5e-2)),
                                f_settings=dict(
                                    xmin=5e-2,
                                    ymin=1e-5,
                                    disable=True),
                                f2_settings=dict(
                                    xmin=1.5e-1,),
                                )

        ax = plt.gca()
        if show_inset:
            # Inset with Strayfield
            with sns.color_palette('deep'):
                inset = inset_axes(ax, width='100%', height='90%',
                                   bbox_to_anchor=(.1, .06, .3, .33),
                                   bbox_transform=ax.transAxes)
                self.m.plot_strayfield(inset, 'Strayfield Plusses',
                                       nolegend=True, )
                inset.legend(['Up',  # ($-M_S \\rightarrow +M_S$)',
                              'Down'])  # ($+M_S \\rightarrow -M_S$)'])
                inset.grid(visible=True, alpha=.4)
                inset.set_xlim(-50, 50)
                inset.set_ylim(-.65, .45)
                inset.set_xticks([-50 + 25 * _ for _ in range(5)])

                y1, y2 = -1, 2
                inset.fill([-25, -25, 25, 25], [y1, y2, y2, y1], 'blue', alpha=.1)
                inset.annotate("", xy=(25, -.35), xytext=(-25, -.2),
                               arrowprops=dict(arrowstyle="->", color='blue'))

        # Inset showing fitted data
        with sns.color_palette("deep"):
            inset2 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=(.5, .75, .3, .25),
                                bbox_transform=ax.transAxes)

            i2data = {}
            for nr, content in to_show.items():
                intercept, slope = self.eva[nr].fit(fit_range=(2e-2, 7e-1))
                sweep_rate = content[2]
                i2data[nr] = {'sweeprate': sweep_rate,
                              'alpha': -slope,
                              'amplitude': 10**intercept}

                inset2.plot(sweep_rate, 10 ** intercept, 'o')

            inset2.set_xlabel(
                'Sweeprate $\\frac{d}{dt} \mu_0 H_{ext} \\left[\\frac{\\mathrm{mT}}{\\mathrm{min}}\\right]$')
            inset2.set_ylabel('$S_{V_H} (f=1\\;$Hz$)$')
            inset2.set_yscale('log')

        # Only save if necessary
        plt.savefig('{}.pdf'.format(filename))
        return i2data

    def fast_sweeprate(self, show_numbers=[320, 325, 323, 329, ], xlim=None,
                       filename='FastSR-1', pos2=False,
                       title='\\Huge \\textbf{a) Large Field Sweeps}',
                       show_inset=False):
        """anas Fast Sweeps over large sweep ranges (1T)

        Args:
            show_numbers:
            xlim:
            filename (TYPE, optional): DESCRIPTION. The default is 'FastSR-1'.
            pos2:
            title:

        Returns:
            None.:
        """
        to_show = {
            320: [(2, 1), 'Empty', 9.4],
            321: [(2, 1), 'Empty', 4.7],
            325: [(2, 1), 'Crosses', 9.4],
            326: [(2, 1), 'Crosses', 4.7],
            323: [(1, -1), 'Empty', 9.4],
            324: [(1, -1), 'Empty', 4.7],
            329: [(1, -1), 'Crosses', 9.4],
            322: [(.5, -.5), 'Empty', 9.4],
            327: [(.5, -.5), 'Crosses', 9.4],
            328: [(.5, -.5), 'Crosses', 4.7],
            336: [(.5, -.5), 'Plusses', 9.4],
            333: [(.75, -.75), 'Crosses', 9.4],
            332: [(.3, -.3), 'Crosses', 9.4],
            331: [(.1, -.1), 'Crosses', 9.4],
        }

        lofm = {}
        for nr, content in to_show.items():
            if content[2] == 4.7:
                continue
            if not (nr in show_numbers):
                continue
            leg_str = '\\textbf{%s} $\\;\\mathbf{%s \\rightarrow %s\\,\\mathrm{T}}$'
            lofm[nr] = leg_str % (
                content[1],   # Structure
                content[0][0],# Field from
                content[0][1],# Field to
                # content[2], # Sweeprate
            )

        self.m.style.set_style(default=True, grid=True, size='talk',
                               style='ticks',
                               latex=True, palette='Paired')
        fig, ax = plt.subplots(figsize=(16, 12))
        for i, j in lofm.items():
            label = 'm%s: %s' % (i, j)
            if False:
                self.eva[i].plot(label=label, ax=ax, color='orange',
                                 dont_set_plot_settings=True)
            else:
                self.eva[i].plot(label=label, ax=ax,
                                 dont_set_plot_settings=True)

        self.eva[i]._set_plot_settings(xlim=(1e-2, 1.6e0), ylim=(1e-7, 5e-2),
                                       title=title,
                                       fontdict={'fontsize': 64,
                                                 'fontweight': 'bold'},
                                       grid=dict(which='minor',
                                                 color='#ffff99',
                                                 linestyle='--', alpha=.5))
        self.eva[i]._draw_oneoverf(ax, ymin=1e-2, xmin=3e-2, alpha=2,
                                   plot_style='b--', an_color='blue')
        # self.eva[i]._draw_oneoverf(ax, xmin=1e-2, ymin=1e-6)
        plt.grid(visible=True, which='minor', color='#cccccc', linestyle='-.',
                 alpha=.3)

        if show_inset:
            # Inset with Strayfield
            with sns.color_palette('deep'):
                if pos2:
                    bbox = (.1, .1, .3, .25)
                else:
                    bbox = (.32, .72, .3, .25)
                inset = inset_axes(ax, width='100%', height='100%',
                                   bbox_to_anchor=bbox,
                                   bbox_transform=ax.transAxes)

                self.m.plot_strayfield(inset, 'Strayfield Crosses',
                                       nolegend=True,
                                       show_plusses=False)
                inset.grid(visible=True, alpha=.4)
                if xlim:
                    inset.set_xlim(*xlim)
                else:
                    inset.set_xlim(-1000, 1000)
                inset.set_ylim(-.8, -1.45)

                if pos2:
                    inset.set_xticks([-300 + 200 * _ for _ in range(4)],
                                     minor=True)
                    for color, x1 in [((202 / 255, 178 / 255, 214 / 255), 300),
                                      ((106 / 255, 61 / 255, 154 / 255), 100)]:
                        y1, y2 = -2, 2
                        inset.plot([x1, x1, -x1, -x1], [y1, y2, y2, y1],
                                   color=color)
                # inset.fill([-500, -500, 500, 500], [y1, y2, y2, y1], 'blue', alpha=.1)
                # inset.annotate("", xy=(25, .35), xytext=(-25, .2),
                #             arrowprops=dict(arrowstyle="->", color='blue'))

        plt.savefig('%s.pdf' % filename)

    def sv_temp_effect(self, filename='sv-temp-effect',
                       show_inset=False):
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True,
                               palette='deep')

        lofm = {}
        to_show = {
            351: [("-M_s \\rightarrow -25", 25), 'Plusses', 1, 30],
            355: [("-M_s \\rightarrow -25", 25), 'Plusses', 0.5, 25],
            356: [("-M_s \\rightarrow -25", 25), 'Plusses', 0.5, 20],
            357: [("-M_s \\rightarrow -25", 25), 'Plusses', 0.5, 15],
            358: [("-M_s \\rightarrow -25", 25), 'Plusses', 0.5, 10],
            359: [("-M_s \\rightarrow -25", 25), 'Plusses', 0.5, 5],
        }

        for nr, content in to_show.items():
            lofm[nr] = ['$%s\\;\mathrm{K}$' % (
                content[3],
            ), {}]
            # self.eva[nr].Vx *= grad

        fig, ax = self.eva.plot(lofm,
                                # fit_range=(2e-2, 7e-1),
                                # show_fit=True,
                                plot_settings=dict(
                                    title='',#Temperature Effect',
                                    xlim=(1e-2, 1.6e0),
                                    ylim=(6e-7, 5e-2)
                                ),
                                f_settings=dict(disable=True),
                                f2_settings=dict(
                                    xmin=7e-2
                                )
                                )

        if show_inset:
            # Inset with Strayfield
            with sns.color_palette('deep'):
                inset = inset_axes(ax, width='100%', height='90%',
                                   bbox_to_anchor=(.75, .4, .25, .25),
                                   bbox_transform=ax.transAxes)
                self.m.plot_strayfield(inset, 'Strayfield Plusses',
                                       nolegend=True, )
                inset.legend(['Up', 'Down'],
                             loc='upper right')
                inset.grid(visible=True, alpha=.4)
                inset.set_xlim(-50, 50)
                inset.set_ylim(-.65, .45)
                inset.set_ylabel('')

                y1, y2 = -1, 2
                inset.fill([-25, -25, 25, 25], [y1, y2, y2, y1], 'blue', alpha=.1)
                inset.annotate("", xy=(25, -.35), xytext=(-25, -.2),
                               arrowprops=dict(arrowstyle="->", color='blue'))

        # Inset showing fitted data
        with sns.color_palette("deep"):
            inset2 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=(.5, .75, .3, .25),
                                bbox_transform=ax.transAxes)
            inset3 = inset_axes(ax, width='100%', height='100%',
                                bbox_to_anchor=(.1, .09, .3, .3),
                                bbox_transform=ax.transAxes)

            i2data = {}
            for nr, content in to_show.items():
                intercept, slope = self.eva[nr].fit(fit_range=(2e-2, 7e-1))
                temp = content[3]
                i2data[nr] = {'temp': temp,
                              'alpha': -slope,
                              'amplitude': 10 ** intercept}

                inset2.plot(temp, 10 ** intercept, 'o')
                inset3.plot(temp, -slope, 'o')

            inset2.set_xlabel('Temperature [$\mathrm{K}$]')
            inset2.set_ylabel('$S_{V_H} (f=1\\;$Hz$)$')
            inset2.set_yscale('log')
            inset2.set_xticks([5 + 10 * _ for _ in range(3)], minor=True)

            inset3.set_xlabel('Temperature [$\mathrm{K}$]')
            inset3.set_ylabel('$\\alpha$')
            inset3.set_xticks([5 + 10 * _ for _ in range(3)], minor=True)

        # Only save if necessary
        plt.savefig('{}.pdf'.format(filename))
        return i2data

    def multiple_fspans(self, filename='multiple-fspans'):
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True,
                               palette='Paired')

        lofm = {}
        to_show = {
            340: [(25, -25), 'Empty', 0.46, {}],
            349: [("-25", 25), 'Empty', 0.5, {}],
            338: [(25, -25), 'Plusses', 0.46, {}],
            342: [("+M_s \\rightarrow 25 \\rightarrow 8.3", -25), 'Plusses',
                  +0.11, {}],
            343: [("-M_s \\rightarrow -25 \\rightarrow -8.3", 25), 'Plusses',
                  0.11, {'color': 'red'}],
        }

        for nr, content in to_show.items():
            if content[2] == 9.4:
                continue
            lofm[nr] = ['$%s \\rightarrow %s$ mT %s ($%s \\frac{mT}{min}$)' % (
                content[0][0],
                content[0][1],
                content[1],
                content[2],
            ), content[3]]

        self.eva.plot(lofm,
                      plot_settings=dict(
                          title='($90^\\circ$) Field Sweeps ($B = \\pm 25 mT$)',
                          xlim=(6e-3, 1.6e0),
                          # ylim=()
                      ),
                      f_settings=dict(disable=True,
                                      xmin=5e-2,
                                      ymin=1e-5),
                      f2_settings=dict(
                          xmin=1e-2, )
                      )

        ax = plt.gca()
        with sns.color_palette('Dark2'):
            inset = inset_axes(ax, width='100%', height='90%',
                               bbox_to_anchor=(.7, .44, .3, .3),
                               bbox_transform=ax.transAxes)
            self.m.plot_strayfield(inset, 'Strayfield Plusses',
                                   show_crosses=False,
                                   nolegend=True)
            inset.legend(['Up', 'Down'])
            inset.grid(visible=True, alpha=.4)
            s = 30
            inset.set_xlim(-s, s)
            inset.set_ylim(-.65, .45)
            s = 20
            inset.set_xticks(np.linspace(-s, s, 5))
            inset.set_xticks([-s - 5 + 10 * _ for _ in range(6)], minor=True)

            y1, y2 = -1, 2
            inset.fill([-25, -25, -8.3, -8.3], [y1, y2, y2, y1], 'red',
                       alpha=.2)
            inset.fill([25, 25, 8.3, 8.3], [y1, y2, y2, y1], 'green', alpha=.2)
            inset.fill([8.3, 8.3, -8.3, -8.3], [y1, y2, y2, y1], 'blue',
                       alpha=.05)
            # inset.plot([8.3, 8.3], [y1, y2], 'b-.', alpha=.8)

        # Only save if necessary
        plt.savefig('{}.pdf'.format(filename))

    def negative_sweeps(self):
        """Testing without file output.

        Returns:
            None.:
        """
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True,
                               palette='Paired')
        lofm = {}
        to_show = {
            340: [(25, -25), 'Empty', 0.46, {}],
            338: [(25, -25), 'Plusses', 0.46, {}],
            348: [("-M_s \\rightarrow 36.56", -291), 'Empty', 0.5, {}],
            347: [("-M_s \\rightarrow 36.56", -291), 'Plusses', 0.5, {}],
        }

        for nr, content in to_show.items():
            if content[2] == 9.4:
                continue
            options = content[3]
            lofm[nr] = ['$%s \\rightarrow %s$ mT %s ($%s \\frac{mT}{min}$)' % (
                content[0][0],
                content[0][1],
                content[1],
                content[2],
            ), options]

        fig, ax = self.eva.plot(lofm,
                                plot_settings=dict(
                                    title='($90^\\circ$) Field Sweeps (measurement Plan \#2)',
                                    xlim=(6e-3, 1.6e0),
                                    # ylim=()
                                ),
                                f_settings=dict(
                                    xmin=6e-3,
                                    ymin=1e-5))

        with sns.color_palette('muted'):
            inset = inset_axes(ax, width='100%', height='90%',
                               bbox_to_anchor=(.28, .73, .29, .24),
                               bbox_transform=ax.transAxes)
            self.m.plot_strayfield(inset, 'Strayfield', nolegend=True)
            inset.legend(['Up ($-M_S \\rightarrow +M_S$)',
                          'Down ($+M_S \\rightarrow -M_S$)'])
            inset.grid(visible=True, alpha=.4)
            inset.set_xlim(-650, 50)
            inset.set_ylim(-.65, .45)

            y1, y2 = -1, 2
            inset.fill([-25, -25, 25, 25], [y1, y2, y2, y1], 'blue', alpha=.1)
            inset.fill([-291.13, -291.13, 36.56, 36.56], [y1, y2, y2, y1],
                       'green', alpha=.1)
            inset.fill([-611, -611, -443, -443], [y1, y2, y2, y1], 'orange',
                       alpha=.1)
            inset.fill([-291, -291, -443, -443], [y1, y2, y2, y1], 'darkred',
                       alpha=.1)

    def measplan12(self):
        c1 = sns.color_palette("hls", 6)
        # sns.color_palette("Reds_r", 14)
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True,
                               palette='deep')
        sns.set_palette(c1)
        lofm = {}
        to_show = {
            383: [("+M_s \\rightarrow 25", -25), 'Plusses', +0.5, {}],
            384: [("-25", -75), 'Plusses', +0.5,
                  {'color': 'orange', 'linestyle': '--'}],
        }

        for i in range(6):
            to_show.update(
                {
                    (385 + i): [("+M_s \\rightarrow %d" % (-(i * 50 + 25)),
                                 (-(i * 50 + 25) - 50)), 'Plusses', +0.5, {}],
                }
            )

        for nr, content in to_show.items():
            if content[2] == 9.4:
                continue
            options = content[3]
            lofm[nr] = ['$%s \\rightarrow %s\;\mathrm{mT}$' % (
                content[0][0],
                content[0][1],
            ), options]

        fig, ax = self.eva.plot(lofm,
                                plot_settings=dict(
                                    title='($90^\\circ$) Field Sweeps (Plusses $0.5\;\mathrm{mT}/\mathrm{min}$)',
                                    xlim=(1e-2, 1.6e0),
                                    ylim=(3e-3, 6e-7)
                                ),
                                f_settings=dict(
                                    xmin=1e-2,
                                    ymin=3e-5),
                                f2_settings=dict(  # disable=True
                                    xmin=2e-2,
                                    plot_style='k-.',
                                    an_color='k'
                                ),
                                )

        ax = plt.gca()
        with sns.color_palette(c1):
            inset = inset_axes(ax, width='100%', height='90%',
                               bbox_to_anchor=(.4, .73, .29, .24),
                               bbox_transform=ax.transAxes)
            self.m.plot_strayfield(inset, 'Strayfield', nolegend=True)
            inset.legend(['Up ($-M_S \\rightarrow +M_S$)',
                          'Down ($+M_S \\rightarrow -M_S$)'])
            inset.grid(visible=True, alpha=.4)
            inset.set_xlim(-650, 50)
            inset.set_ylim(-.65, .45)

            y1, y2 = -1, 2
            for j in [25] + [-25 - 50 * i for i in range(
                    5)]:  # + [-300-50*i for i in range(5)] + [-575]:
                inset.fill([j, j, j - 50, j - 50], [y1, y2, y2, y1], alpha=.4)
            # for j in [-300-50*i for i in range(5)] + [-575]:
            #    inset.fill([j, j, j-50, j-50], [y1, y2, y2, y1], alpha=.4)
        ax.annotate('Without Saturation', (1.2e-2, 1e-3), color='orange',
                    rotation=-55)
        ax.annotate('With Saturation', (1.01e-2, .8e-3), color='yellow',
                    rotation=-37)

        plt.savefig('self.measplan12-1.pdf')

    def measplan12_2(self):
        c1 = sns.color_palette("hls", 7)
        # sns.color_palette("Reds_r", 14)
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True,
                               palette='deep')
        sns.set_palette(c1)
        lofm = {}
        to_show = {}
        for i, j in enumerate([-(_ * 50 + 300) for _ in range(5)] + [-575]):
            to_show.update(
                {
                    (391 + i): [("+M_s \\rightarrow %d" % (j), (j - 50)),
                                'Plusses', +0.5, {}],
                }
            )

        j = -625
        to_show[397] = [("-M_s \\rightarrow %d" % (j), (j + 50)), 'Plusses',
                        +0.5, {}]

        for nr, content in to_show.items():
            if content[2] == 9.4:
                continue
            options = content[3]
            lofm[nr] = ['$%s \\rightarrow %s\;\mathrm{mT}$' % (
                content[0][0],
                content[0][1],
            ), options]

        fig, ax = self.eva.plot(lofm,
                                plot_settings=dict(
                                    title='($90^\\circ$) Field Sweeps (Plusses $0.5\;\mathrm{mT}/\mathrm{min}$)',
                                    xlim=(1e-2, 1.6e0),
                                    ylim=(2e-4, 2e-7)
                                ),
                                f_settings=dict(
                                    xmin=1e-2,
                                    ymin=2e-5),
                                f2_settings=dict(  # disable=True
                                    xmin=1e-2,
                                    plot_style='k-.',
                                    an_color='k',
                                    disable=True,
                                ),
                                )

        with sns.color_palette(c1):
            inset = inset_axes(ax, width='100%', height='90%',
                               bbox_to_anchor=(.4, .73, .29, .24),
                               bbox_transform=ax.transAxes)
            self.m.plot_strayfield(inset, 'Strayfield Plusses', nolegend=True)
            inset.legend(['Up ($-M_S \\rightarrow +M_S$)',
                          'Down ($+M_S \\rightarrow -M_S$)'])
            inset.grid(visible=True, alpha=.4)
            inset.set_xlim(-650, 50)
            inset.set_ylim(-.65, .45)

            y1, y2 = -1, 2
            for j in [-300 - 50 * i for i in range(5)] + [-575]:
                inset.fill([j, j, j - 50, j - 50], [y1, y2, y2, y1], alpha=.4)

        plt.savefig('self.measplan12-2.pdf')

    def measplan12_full(self, name='measplan12-full'):
        """measurement Plan #12 All 14 measurements in one Graph.

        Args:
            name:
        """
        # c1 = sns.color_palette("twilight", 14)
        c1 = sns.color_palette("hls", 14)
        self.m.style.set_style(default=True, grid=True,
                               size='talk', style='ticks', latex=True)
        sns.set_palette(c1)

        # Choosing measurement Numbers to plot
        lofm = {}
        to_show = {
            383: [("+M_s \\rightarrow 25", -25), 'Plusses', +0.5, {}],
            384: [("-25", -75), 'Plusses', +0.5,
                  {'color': 'orange', 'linestyle': '--'}],
        }

        for i in range(6):
            to_show.update(
                {
                    (385 + i): [("+M_s \\rightarrow %d" % (-(i * 50 + 25)),
                                 (-(i * 50 + 25) - 50)), 'Plusses', +0.5, {}],
                }
            )

        for i, j in enumerate([-(_ * 50 + 300) for _ in range(5)] + [-575]):
            to_show.update(
                {
                    (391 + i): [("+M_s \\rightarrow %d" % (j), (j - 50)),
                                'Plusses', +0.5, {}],
                }
            )

        j = -625
        to_show[397] = [("-M_s \\rightarrow %d" % (j), (j + 50)), 'Plusses',
                        +0.5, {}]

        # Converting them to a list of measurements (lofm)
        for nr, content in to_show.items():
            if content[2] == 9.4:
                continue
            options = content[3]
            lofm[nr] = ['$%s \\rightarrow %s\;\mathrm{mT}$' % (
                content[0][0],
                content[0][1],
            ), options]

        fig, ax = self.eva.plot(lofm,
                                plot_settings=dict(
                                    title='($90^\\circ$) Field Sweeps (Plusses;' + \
                                          ' $\\Delta B = 50\\mathrm{mT}$;' + \
                                          ' $0.5\\;\\mathrm{mT}/\\mathrm{min}$)',
                                    xlim=(1e-2, 1.6e0),
                                    ylim=(5e-2, 5e-8),
                                    legend_settings=dict(loc='best', ncol=2)
                                ),
                                f_settings=dict(
                                    xmin=1e-2,
                                    ymin=3e-5),
                                f2_settings=dict(  # disable=True
                                    xmin=2e-2,
                                    plot_style='k-.',
                                    an_color='k'
                                ),
                                )

        with sns.color_palette(c1):
            inset = inset_axes(ax, width='100%', height='100%',
                               bbox_to_anchor=(.07, .06, .34, .26),
                               bbox_transform=ax.transAxes)
            self.m.plot_strayfield(inset,
                                   'Strayfield Plusses',
                                   show_plusses=False,
                                   show_crosses=False,
                                   nolegend=True)

            y1, y2 = -1, 2
            for j in [25 - 50 * i for i in range(6)] + \
                     [-300 - 50 * i for i in range(5)] + [-575]:
                inset.fill([j, j, j - 50, j - 50], [y1, y2, y2, y1], alpha=.8)

            inset.plot(self.m.up_fitted.B, self.m.up_fitted.Bx8, 'b-',
                       linewidth=1.5, label='Up')
            inset.plot(self.m.down_fitted.B, self.m.down_fitted.Bx8, 'r-',
                       linewidth=3, label='Down')

            inset.legend(loc='best')

            inset.grid(visible=True, alpha=.4)
            inset.set_xlim(-650, 50)
            inset.set_ylim(-.65, .45)
            inset.set_xlabel('')
            inset.set_ylabel('')

        ax.annotate('Without Saturation', (1.2e-2, 5e-4), color='orange',
                    rotation=-55)
        ax.annotate('With Saturation', (1.01e-2, 4e-4), color='orange',
                    rotation=-35)

        plt.savefig('%s.pdf' % name)

    def set_size(self, width_pt, fraction=1, subplots=(1, 1)):
        """Set figure dimensions to sit nicely in our document.
        
        Source: https://jwalton.info/Matplotlib-latex-PGF/

        Parameters
        ----------
        width_pt: float
                Document width in points
        fraction: float, optional
                Fraction of the width which you wish the figure to occupy
        subplots: array-like, optional
                The number of rows and columns of subplots.
        Returns
        -------
        fig_dim: tuple
                Dimensions of figure in inches
        """
        # Width of figure (in pts)
        fig_width_pt = width_pt * fraction
        # Convert from pt to inches
        inches_per_pt = 1 / 72.27

        # Golden ratio to set aesthetic figure height
        golden_ratio = (5 ** .5 - 1) / 2

        # Figure width in inches
        fig_width_in = fig_width_pt * inches_per_pt
        # Figure height in inches
        fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

        return (fig_width_in, fig_height_in)
