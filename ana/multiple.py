# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Main parent class for all multiple measurements.
These Measurements are represent by one data variable.

All multiple measurements have a variable called ``multi_info`` which contains
a pandas.DataFrame with parameter settings to single measurements.

All single can be represented by

    >>> m = ana.MultipleM()
    >>> m
    Nr. 0: Hloop
    Nr. 0: RAW
"""

from .measurement import MeasurementClass

import os
import pandas as pd

class MultipleM(MeasurementClass):
    def __init__(self):
        """MultipleM:

        Main parent class for all multiple measurements. These Measurements
        are represent by one data variable.

        All multiple measurements have a variable called ``multi_info`` which
        contains a pandas.DataFrame with parameter settings to single
        measurements.

        All multiple measurements can be represented by

            >>> m = ana.MultipleM()
            >>> m
            Nr. 0: Hloop
            Nr. 0: RAW
        """
        
        super().__init__()
        self.multi_info = pd.DataFrame()
        if os.path.exists('data/mfn_info.csv'):
            self.read_csv('data/mfn_info.csv')
    
    
    query = lambda self, request: self.multi_info.query(request)

    def get_lofm_sweeps(self, to_show, label, options={}):
        """Workaround to get list of measurements for sweeps.

        Args:
            to_show (dict): Measurement Numbers pointing to label format
                variables: e.g. `` {nr: [-1, 1, 'Plusses', '2 mT/min']}
            label (str): Label to show in plots: e.g. "Show %s -> %s (%s) %s "
                -> will be formated using to_show
            options (dict, default: empty): Additional plotting options

        Returns:
            dict: List of Measurements with description
        """
        
        return self.get_lofm(to_show, 
                             label, 
                             options)

    def read_csv(self, *args, **kwargs): 
        """Updates multi_info DataFrame. Reads csv and adds information to
        self.multi_info

        Args:
            *args:
            **kwargs:

        Returns:
            None
        """

        self.multi_info = pd.concat([self.multi_info, pd.read_csv(*args, **kwargs)])

    def get_lofm(self, to_show, label, options={}):
        """Returns the List of Measurements (lofm) for plot functions
            (see :meth:`ana.HandleM.plot`).

        Args:
            to_show (dict): Measurement Numbers pointing to label format
                variables: e.g. ``{nr: [(-1, 1), 'Plusses', '5 mT/min']}``
            label (str): Label to show in plots: e.g.
                ``"Show %s -> %s (%s) %s "`` -> will be string formated using
                to_show
            options (dict, optional, default: empty): Additional plotting
                parameters

        Returns:
            dict: List of Measurements with description: ``{nr: [labels]}``
        """
        lofm = {}
        for nr, content in to_show.items():
            if isinstance(content[0], tuple):
                form = content[0][0], content[0][1], content[1], content[2]
            # elif isinstance(content[0], str) or \
            #         isinstance(content[0], int) or \
            #         isinstance(content[0], float):
            #    form = content[0], content[1], content[2]
            else:
                form = content
                
            lofm[nr] = [label % form, options]

        return lofm
