# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
SingleMeasurement
=================

Main parent class for all single measurements.
These Measurements are represent by one DataFrame.
"""

from .measurement import MeasurementClass

import pandas as pd
import scipy.stats


class SingleM(MeasurementClass):
    def __init__(self):
        """The main constructor for all single measurements."""
        super().__init__()
        self.data = pd.DataFrame()

    linear_regression = lambda self, x, y: scipy.stats.linregress(x, y)
    
    def fit_variable(self, df, x, y):
        """Applys a linear regression and adds result to DataFrame

        Args:
            df (pd.DataFrame): Fit data
            x (np.ndarray): Fit variable.
            y (np.ndarray): Fit variable.
        """
        fit = self.linear_regression(x[1], y[1])
        for var in [x, y]:
            df[var[0]] = var[1]
    
        return df