# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
================
SA
================

Evaluate data from signal analyzer.
"""

from .single import SingleM

import logging
import numpy as np
import pandas as pd
import scipy.stats

import matplotlib.pyplot as plt


class SA(SingleM):
    """Loads and analyzes datafiles from :instrument:`SR785`"""
    def __init__(self, *args, **kwargs):
        """
        Args:
            *args:
            **kwargs:

        Returns:
            None

        Raises:
            * ValueError
        """
        super().__init__()
        self.logger = logging.getLogger('SA')
        self.name = 'SA'
        try:
            self.data = kwargs.get('data', args[0])
        except Exception:
            error_message = "Not Able to find DataFrame. Please use SA_measurement(data)."
            self.logger.critical(error_message)
            raise ValueError(error_message)

        if isinstance(self.data, dict):
            self.df = self.data.get('data')
            self.info = self.data.get('info')
        else:
            self.df = self.data

        # Convert data to numeric values
        try:
            self.df.f = pd.to_numeric(self.df.f, errors='coerce')
            self.df.Vx = pd.to_numeric(self.df.Vx, errors='coerce')
            self.df.Vy = pd.to_numeric(self.df.Vy, errors='coerce')
        except Exception:
            error_message = "Unable to convert data to numeric."
            self.logger.critical(error_message)
            raise ValueError(error_message)

        # Redirect functions
        self._draw_oneoverf = self.style.draw_oneoverf
        self._set_plot_settings = self.style.set_plot_settings
        self._calc_log = self.calc_log

    def __repr__(self):
        ret = 'SA Measurement (Nr. %s)\n' % self.info.get('Nr', 0)
        for key, val in self.info.items():
            ret += '%s:\t%s\n' % (key, val)
        return ret


    def plot(self, **kwargs):
        """Plotting the Data on given Axis or creating a new Plot for plotting.

        .. code-block:: python
           :linenos:
           :caption: Signature

           plot(ax, label=None, color=None, linestyle=None,
               plot_x='f', plot_y='Vx', dont_set_plot_settings=False,

                   xscale='log', yscale='log', xlim=(None, None), ylim=(None,
                   None), legend_location='best', grid=None, title=None,
                   xlabel='f [Hz]', ylabel='S_VH [V2/Hz]',

           )

        Args:
            **kwargs:

        Note:
            If **dont_set_plot_settings** is True, all kwargs below are not
            used.
        """
        ax = kwargs.get('ax')
        if not ax:
            fig, ax = plt.subplots()

        plotargs = {}
        if 'label' in kwargs:
            plotargs['label'] = kwargs.get('label')
        if 'color' in kwargs:
            plotargs['color'] = kwargs.get('color')
        if 'linestyle' in kwargs:
            plotargs['linestyle'] = kwargs.get('linestyle')

        ax.plot(self.df[kwargs.get('plot_x', 'f')],
                self.df[kwargs.get('plot_y', 'Vx')],
                **plotargs)

        if not (kwargs.get('dont_set_plot_settings', False)):
            self._set_plot_settings(**kwargs)

    def fit(self, fit_range=(1e-2, 7e-1)):
        """Fits a linear regression

        Args:
            fit_range (tuple, optional, default: (1e-2, 1e0).):
        """
        self.calc_log(self.df, ['f', 'Vx', 'Vy'])
        xmin, xmax = fit_range

        fit_area = self.df[self.df['f'] < xmax]
        fit_area = fit_area[fit_area.f > xmin]
        f = scipy.stats.linregress(fit_area.lnf, fit_area.lnVx)

        self.info['Slope'] = f.slope
        self.info['Intercept'] = f.intercept
        return f.intercept, f.slope
