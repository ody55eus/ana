# SPDX-FileCopyrightText: 2020/2021 Jonathan Pieper <ody55eus@mailbox.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Helps to handle measurements and style plots.
"""

from glob import glob

from functools import wraps
from time import time

from .plotting import PlottingClass
from .measurement import MeasurementClass

plot = PlottingClass()
set_sns = plot.set_style
set_plot_settings = plot.set_plot_settings

meas = MeasurementClass()
get_info_from_name = meas.get_info_from_name


def getpath(*arg):
    """Alias for ``glob``.

    Args:
        *arg:
    """
    return glob(*arg)


def timing(f):
    """Wraps a function to measure the time it takes.

    Args:
        f:
    """
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        te = time()
        print('func:%r took: %2.4f sec' % \
          (f.__name__, te-ts))
        return result
    return wrap
